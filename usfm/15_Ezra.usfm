\id EZR
\h Esdra
\toc1 Buku ya Esdra
\toc2 Esdra
\toc3 Esd
\mt1 Buku ya Esdra
\imt Boyingisi
\is Mokomi
\ip Atako o moi mwa lelo Buku ya Esdra mpe Neemia mazali buku ibale ikeseni ; iye tokobengaka buku ya yambo mpe ya mibale ya Esdra o Vulgate, izali bobele biteni bya ekela yoko ya mokomi. O libandela izalaka se buku yoko. Esdra mpe Neemia bazali bato babale baye bamonisami o babuku ina ibale, bango babale bazalaki se na likanisi lya kosala mpo ya bolamu bwa ekolo mpe bomoi bwa losambo. Atako misala mya ba­ngo babale mikomami te o minka­nda mya Bondeko bwa Kala bwa Ebrei, Esdra amonisamaki lokola nganga mpe moteyi alenda o makambo matali mobeko, moba­ndisi wa bizaleli bya sika mpe moto wa bopi­kiliki, wa makasi epai ya bapagano. Neemia azalaki molaiki wa mpiko, atonda na molende moye moto mosusu azalaka na mwango te, azalaka koteya se na ndakisa ya ye moko, makambo ma nzoto amotali pamba, ye moto wa losambo mpe wa boyambi. Soki Buku ya Esdra mpe Neemia te mbele ezali mpasi mpo ya koyeba maka­mbo masalaka ba-Yuda nsima ya boombo. 
\is1 Eleko 
\ip Buku iye ibale izwami o eleko eye elandi bozongi bwa ba-Yuda ntango bautaka o boombo o mboka Babilon mpe elandami kino mbula nkama moko. Lokola buku iye izali koyebisa biso lolenge batongaki lisusu Te­mpelo mpe mosala Esdra na Neemia basalaka ntango bautaki o Yuda, ekoki kozala solo ’te babuku yango (to mpe buku ina) isalemaki o ekolo ya Palestina. Tokoki kokokisa eleko ya bokomi yango mosika mpe­nza nsima ya maye yango ezali koloba. Na botalaka ’te nganga Nzambe wa suka oyo batangi asalaka mosala o eleko ya ba-Persi, na tembe te tokoloba ’te bakomaka babuku yango o bileko bya eteni ya inei ya mibu nkama inei yambo ya mbotama ya Yezu Kristu.
\is1 Misala 
\ip Buku ya Esdra mpe Neemia isali se eloko yoko, masolo mpe mateya bozindo ma ya­ngo makosala ’te ekomi mpasi mpo ya kokabola yango. O Biblia ya ba-Ebrei, batii yango o liboso lya Buku ya Mikolo nzoka­nde o LXX, yango iyei nsima ya Buku ya Mikolo. Na bokeseni na mibeko mya lelo mya Canon ya Biblia, basengeli kosolola mpe kotiya Buku ya Neemia liboso lya Buku ya Esdra. Kasi lolenge minkanda mimonisi biso, molongo mosengeli kozala moye : Esdra moto akomaka moto wa liboso o Yeruzalem o mobu 458 yambo ya mbotama ya Yezu Kristu, mobu mwa 7 o bokonzi bwa Artaksasta i (Esd 7,8) ; Neemia amolandi o mobu 445, o mobu mwa 20 o bokonzi bwa nkumu ona (Ne 2,1) ; afandi wana mibu 12 (Ne 13,6), elingi koloba kino mobu 433. Akei o ekolo ya Persi, azongi lisusu o Yeruzalem se o bokonzi bwa Artaksasta i. 
\is1 Mwango 
\ip Eteni 1 : monkanda mwa Siro. Eteni 2 : nkombo ya baye bazalaki o boo­mbu. Eteni 3 : bobongisi lolenge la kosambela. Eteni 4,1–5 : banguna ba Yuda bapikami. Eteni 4,6–24 : bokomelani minkanda o bo­konzi bwa Xexes mpe Artaksasta. Eteni 5,1 - 6,18 : botongi ndako ya Nza­mbe. Eteni 6,19–22 : Pasika. Eteni 7,1–10 : mosala mwa Esdra. Eteni 8,1–14 : baninga ba Esdra. Eteni 8,15–36 : mobembo mwa Esdra o Yeruzalem. Eteni 9,1-15 : losambo la bomikitisi bwa Esdra. Eteni 10,1–17 : bozongisi basi bapaya. Eteni 10,18–44 : molongo mwa bato basali mabe.
\c 1
\s1 Etinda ya Siro
\p
\v 1 O mobu mwa yambo Siro azalaki mokonzi wa ba-Persi *, Yawe akokisi maloba ma profeta Yeremia * : apesi Siro, mokonzi wa ba-Persi makanisi malamu ma kosakola mobeko moye o mokili mwa ye mobimba ; ayebisi mwango na monkanda moye :
\v 2 « Maloba ma Siro, mokonzi wa ba-Persi : Yawe Nzambe wa likolo apesi ngai bokonzi o bikolo binso bya nse. Ye moto atindi ngai natongela ye Te­mpelo ya Yeruzalem o mokili mwa Yuda.
\v 3 Yawe azala na bato banso ba ekolo ya ye ! Bakende o Yeruzalem o ekolo ya Yuda, batongela Yawe Nza­mbe wa Israel Tempelo ; Nzambe oyo akofandaka o Yeruzalem.
\v 4 O bisika binso bato bafandi na ba-Yuda esika yoko, basalisa bango na palata, na wo­lo na biloko bya mosala mpe na ngombe ; bakabela bango mpe mabo­nza ma kobonza o Tempelo ya Nza­mbe o Yeruzalem. »
\v 5 Bongo bakolo ba mabota ma Yuda mpe ma Benyamin, banganga Nza­mbe na ba-Levi, baye banso, Nza­mbe apesi mayele malamu ma kozonga o mboka, bakei kotonga Tempelo ya Yawe o Yeruzalem.
\v 6 Mpe baninga ba­nso bayei kosalisa bango ; bapesi biloko bya palata na wolo, na biloko bisusu, mpe bibwele na mabonza ma motuya ndenge na ndenge, moto na moto na motema mwa ye moko.
\v 7 Mokonzi Siro azongisi biloko bya Tempelo ya Yawe, biye Nabukodonozor ayibaki o Yeruzalem mpe atiaki o tempelo ya nzambe wa ye.
\v 8 Siro, mokonzi wa Persi, atindi Mitridate, mokengeli wa mosolo mwa leta, azo­ngisa biloko mpe apesa byango o maboko ma Sesbasar *, nkumu wa Yuda.
\v 9 Tala motango mwa biloko binso : sani ya wolo ntuku isato, sani ya palata nkoto yoko, mbeki ntuku ibale na libwa,
\v 10 nkeni ya wolo ntuku isato na nkeni isusu ya palata nkama inei na zomi, na biloko bisusu ndenge na ndenge nkoto yoko.
\v 11 Motango mwa biloko binso bya wolo na palata mokoki nkoto itano na nkama inei. Sesbasar azongisi biloko bina binso o mokolo bato, baye bazalaki baombo o Babilon, bazongi o Yeruzalem.
\c 2
\s1 Baye bazalaki o boombo
\p
\v 1 Tala awa nkombo ya bana ba ekolo Israel bauti o boombo, baye Nabukodonozor, mokonzi wa Babilon, akangaki mpe atindaki o Babilon ; bazongi o Yeruzalem mpe o Yuda, moto na moto o mboka ya ye *.
\v 2 Baye bazongi bakambami na Zorobabel, Yozue, Neemia, Seraya, Relaya, Mordokai, Bilsan, Mispar, Bigvai, Rekum na Baana. Motango mwa bana ba ekolo Israel :
\p
\v 3 Bana ba Paros : 2 172 ;
\v 4 bana ba Sefatia : 372 ;
\v 5 bana ba Ara : 775 ;
\v 6 bana ba Pakat-Moab, ezali bana ba Yozue na Yoab : 2 812 ;
\v 7 bana ba Elam : 1 254 ;
\v 8 bana ba Zatu : 945 ;
\v 9 bana ba Zakai : 760 ;
\v 10 bana ba Bani : 642 ;
\v 11 bana ba Bebai : 623 ;
\v 12 bana ba Azgad : 1 222 ;
\v 13 bana ba Adonikam : 666 ;
\v 14 bana ba Bigvai : 2 056 ;
\v 15 bana ba Adin : 454 ;
\v 16 bana ba Ater, nkombo ya ye esusu, Yekiskia : 98 ;
\v 17 bana ba Besai : 323 ;
\v 18 bana ba Yora : 112 ;
\v 19 bana ba Kasum : 223 ;
\v 20 bana ba Gibar : 95 ;
\v 21 bato ba Beteleme : 123 ;
\v 22 bato ba Netofa : 56 ;
\v 23 bato ba Anatot : 128 ;
\v 24 bato ba Asmavet : 42 ;
\v 25 bato ba Kiriat-Yearim, Kefira na Beerot : 743 ;
\v 26 bato ba Rama na Geba : 621 ;
\v 27 bato ba Mikmas : 122 ;
\v 28 bato ba Betel na Ayi : 223 ;
\v 29 bana ba Nebo : 52 ;
\v 30 bana ba Magbis : 156 ;
\v 31 bana ba Elam mosusu : 1 254 ;
\v 32 bana ba Karim : 320 ;
\v 33 bana ba Lod, Kadid na Ono : 725 ;
\v 34 bato ba Yeriko : 345 ;
\v 35 bato ba Sena : 3 630.
\v 36 Banganga Nzambe : bana ba Yedaya, ba libota lya Yozue : 973 ;
\v 37 bana ba Imer : 1 052 ;
\v 38 bana ba Pasekur : 1 247 ;
\v 39 bana ba Karim : 1 017.
\v 40 Ba-Levi : bana ba Yozue mpe ba Kadmiel, ba libota lya Odavia : 74.
\v 41 Bayembi : bana ba Asaf : 128.
\v 42 Bakengeli ba bizibeli bya Tempelo : bana ba Salum, Ater, Talmon, Akub, Katita na Sobai : 139.
\v 43 Basali ba Tempelo : bana ba Sika, Kasufa, Tabaot,
\v 44 Keros, Sia, Padon,
\v 45 Lebana, Kagaba, Akuf,
\v 46 Kagab, Samlai, Kanan,
\v 47 Gidel, Gakar, Reaya,
\v 48 Resin, Nekoda, Gazam,
\v 49 Uza, Pasea, Besai,
\v 50 Asna, Meunim, Nefusim,
\v 51 Bakbuk, Kakufa, Karkur,
\v 52 Ba­slut, Mekida, Karsa,
\v 53 Barkos, Sisera, Tema,
\v 54 Nesia na Katifa.
\v 55 Bana ba baombo ba Salomo : bana ba Sotai, Asoferet, Peruda,
\v 56 Yaala, Darkon, Gidel,
\v 57 Sefatia, Katil, Pokeret-Asebaim na Ami.
\v 58 Basalisi na bana ba baombo ba Salomo bazalaki 392.
\v 59 Kasi bato bauti o Tel-Mela, Tel-Karsa, Kerub, Adan na Imer, bakokaki kolakisa polele te ’te bazalaki bato ba mabota to ekolo Israel.
\v 60 Bango baye : bana ba Delaya, Tobia na Nekoda : 652.
\v 61 Se bongo banganga Nza­mbe : bana ba Kobaya, Akos na Barzilai ; oyo abalaki mwasi wa libota lya Barzilai, bongo akamataki nkombo ya ye.
\v 62 Awa banganga Nzambe baye baluki nkombo ya bankoko ba bango o minkanda, kasi bazwi myango te ; yango wana balongoli bango o mosala mwa nganga Nzambe.
\v 63 Bongo mo­yangeli * apekisi bango kolia biloko bisantu tee mokolo nganga Nza­mbe moko amononi, oyo akoki kotuna Urim na Tumim.
\v 64 Banso bazalaki bato 42 360.
\v 65 Bakisa naino baombo ba bango babali na basi : bakoki 7 337. Bayembi babali na basi bazalaki 200.
\v 66 Bazalaki na farasa 736, na mpunda ya mosala 245,
\v 67 na kamela 435 mpe na mpunda 6 720.
\v 68 Ekomi bango o Tempelo ya Yawe o Yeruzalem, bankumu basusu bapesi makabo mpo ya kotonga lisusu Tempelo o esika ezalaka kala.
\v 69 Bapesi lokola bakoki mpe batii binso o ebombelo ya mosolo mwa ndako ya Nzambe : bapesi dragma ya wolo 61 000, bibende bya palata 5 000 na mpe milato mya nganga Nzambe 100.
\v 70 Banganga Nzambe, ba-Levi na ba­to basusu bakei kofanda o Yeruzalem ; bayembi, bakengeli ba bizibeli na basali basusu ba Tempelo na ba-Israel basusu banso bakei kofanda o bingumba bya bango.
\c 3
\s1 Babongisi altare ya kala
\p
\v 1 Ekomi sanza ya nsambo, ba-Israel banso basili bakomi o bi­ngumba bya bango, banso bayei kosangana o Yeruzalem motema moko.
\v 2 Yozue, mwana wa Yosada, na ba­ndeko ba ye banganga Nzambe, na Zorobabel, mwana wa Sealtiel, na bandeko ba ye babandi kotonga lisusu altare ya Nzambe wa Israel mpo ’te bakoka kobonza mabonza lokola ekomami o buku ya Moze, moto wa Nzambe.
\v 3 Batongi lisusu altare o esika ya kala, ata bazalaki kobanga bato batikalaki kofanda wana *. Bongo ba­bonzeli Yawe mabonza ma kotu­mba na ntongo mpe na mpokwa.
\v 4 Ba­se­peli eyenga ya Biema lokola ekomami : mokolo na mokolo babonzi mabonza ma kotumba, se lokola etindami.
\v 5 Ut’o ntango ena bazalaki kobonza libonza lya kotumba lya mokolo na mokolo, na mabonza o mikolo sanza ya sika ekobimaka, mpe o mikolo mya biyenga binso bya Yawe ; bakisa mabonza maye moto na moto alingi kobonzela Yawe o nkombo ya ye moko.
\v 6 Banda mokolo mwa yambo mwa sanza ya nsambo babandi kotumbela Yawe mabonza, ata batii naino mabanga ma nse te mpo ya koto­nga Tempelo ya Yawe lisusu.
\v 7 Bafuti basali mpo bakata mabanga mpe bapasola mabaya ; bapesi bato ba Sidoni na Tiro biloko bya bolei mpe bya bomeli, na mafuta mpo ’te baya na nzete ya sédere ya Libano, balekisa yango o nzela ya mbu tee Yafa *, se lokola Siro, mokonzi wa Persi, apesaki bango ndingisa.
\s1 Balengeli kotonga Tempelo lisusu
\p
\v 8 O mobu mwa mibale mwa bozo-ngi o Tempelo ya Nzambe o Yeruzalem, o sanza ya ibale, Zorobabel, mwana wa Sealtiel, na Yozue, mwana wa Yosada, babandi mosala mwango. Bandeko basusu na banganga Nza­mbe na ba-Levi, baye bautaki o boombo mpe bazongi o Yeruzalem, basalisi bango. Baponi ba-Levi, baye bakokisi mibu ntuku ibale, mpo ’te bakamba misala mya Tempelo ya Yawe.
\v 9 Yozue na bana na bandeko ba ye, Kadmiel na bana na bandeko ba ye, bango banso lisanga bayangeli basali baye batongi Tempelo ya Nza­mbe ; bakisa bana ba Enadad, na bana na bandeko ba bango, ba-Levi.
\v 10 Nta­ngo batongi batii mabanga manene ma nse ma Tempelo ya Yawe, mpe banganga Nzambe balati molato mwa bonganga, babeti mindule, mpe ba-Levi, bana ba Asaf, babeti migbala mpo ya kokumisa Yawe se lokola Davidi, mokonzi wa Israel, atindaki.
\v 11 Bayembi nzembo ya kokumisa mpe kotondo Yawe : « Yawe azali malamu, bolingi akolingaka Israel bokosilaka te *. » Mpe bato banso bagangi na mo­ngongo makasi, bakumisi Yawe mpo ya ebandela ya botongi Tempelo ya Yawe.
\v 12 Banganga Nzambe baike, na ba-Levi na bankumu ba mabota, baye bakomi babange, baye bamonoko Tempelo ya kala, baleli mingi o ntango bamoni ’te bazalaki kotia mabanga manene ma nse. Bato basusu bagangi makasi mpo ya esengo.
\v 13 Kasi boleli boyokani lisusu te, mpo makeléle ma bato bazalaki koganga na esengo mayokani mosika.
\c 4
\s1 Songisongi ya bato ba Samaria
\p
\v 1 Eyoki banguna ba Yuda mpe ba Benyamin * ’te bato bautaki o boombo babandi kotonga Tempelo ya Yawe Nzambe wa Israel,
\v 2 bakei epai ya Zorobabel na bankumu ba mabota, balobi na bango : « Tolingi kotonga na bino elongo, mpo biso mpe tokokumisaka Nzambe wa bino. Tokobonzelaka ye mabonza ba­nda eleko ya Asaradon, mokonzi wa Asur, oyo atindaki biso awa. »
\v 3 Zorobabel, Yozue na bakonzi ba mabota ba Israel bayanoli : « Ebongi te ’te biso totongela Nzambe wa biso Te­mpelo na bino elongo. Biso moko tokotongela Yawe Nzambe wa Israel ndako, lokola Siro, mokonzi wa Persi, atindaki biso. »
\v 4 Bato ba mokili babandi kolembisa mitema mya bato ba Yuda mpe babangisi bango mpo batonga lisusu te.
\v 5 Bafuti balaki mpo ’te bakoka kokokisa mokano mwa ba­ngo te ; basali bongo mikolo minso mya bokonzi bwa Siro *, mokonzi wa Persi, tee o eleko Dario akomi mokonzi wa sika o esika ya ye.
\v 6 O eleko Serses abandi koyangela ekolo, se bango bakomi monkanda mpo ya kofunda bato ba Yuda mpe ba Yeruzalem *.
\v 7 O eleko Artaksasta azalaki mokonzi, Bislam, Mitridate, Tabeel na baninga ba bango basusu bakomeli Artaksasta, mokonzi wa Persi, mo­nkanda ; bakomi mwango na nkoma mpe o lokota la ba-Aram.
\v 8 Rekum *, moyangeli wa Samaria, na Simsai, mokomi monkanda, bakomeli mokonzi Artaksasta monkanda moye mpo ya kofunda bato ba Yeruzalem, bakomi boye :
\v 9 « Biso moyangeli Rekum na mokomi monkanda Simsai na baninga ba biso basusu, bazuzi na bantoma na bakomanda basusu ba Persi, bato ba Uruk, Babilon na Suza o Elam,
\v 10 na bato ba bikolo bisusu, baye mokonzi monene mpe wa lokumu Asurbanipal akangaki mpe afa­ndisi * o Samaria mpe o bingumba bisusu o ngambo ya Efrate. »
\v 11 Tala awa maloba ma monkanda batindeli ye : « Biso basaleli ba yo na bato banso bafandi o ngambo ya Efrate, tokomeli yo, mokonzi Artaksasta, monka­nda moye :
\v 12 Tosengeli koyebisa mokonzi ’te ba-Yuda, baye bautaki epai ya yo mpe bakomi epai ya biso o Yeruzalem, bazali kotonga lisusu engu­mba eye ya batomboki ; babandi kobongisa lopango lonene mpe kotia mabanga ma nse.
\v 13 Sikawa, ebongi mokonzi ayeba ’te soko batongi ya­ngo lisusu mpe babongisi lopango lonene, mokolo mosusu bakofuta lisusu ntako, mpako mpe ndingisa ya koleka te, bongo mosolo mokokoto lisusu o ebombelo ya leta te.
\v 14 Awa tozali basaleli ba mokonzi, tolingi lokumu la mokonzi lobeba te. Yango wana toyebisi mokonzi makambo maye,
\v 15 mpo batala minkanda mya Bikela bya bankoko ba yo. O mi­nkanda mina, okomono mpe okoyeba ’te bato ba engumba eye bazali batomboki mpe bakotiaka matata na bakonzi mpe na bikolo bisusu, bakolukaka kobulinginya bato ut’o kala ; se na ntina ena engumba eye ebebaki.
\v 16 Toyebisi yo mokonzi ’te soko bato­ngi engumba eye lisusu mpe babo­ngisi lopango, mosika te, okobungisa bokonzi bwa yo o ngambo ya Efrate. »
\v 17 Mokonzi atindeli bango eyano eye : « Boboto na yo, moyangeli Rekum, na mokomi minkanda Simsai, mpe na baninga ba bino banso bakofandaka o Samaria mpe o ngambo ya Efrate.
\v 18 Tozwi monkanda botindelaki ngai ; o miso ma ngai batangi mpe babo-ngoli mwango o lokota la biso.
\v 19 Natindi bato ba ngai batala minkanda mya Bikela mpe bamoni ’te ut’o kala bato ba engumba eye batombokoki na bakonzi, babimisaki mpe mobulu na matata.
\v 20 Bakonzi banene bazalaka kala o Yeruzalem mpe bayangelaka mokili mobimba o ngambo ya Efrate ; kuna bato bazalaki kofuta ntako, mpako mpe ndingisa ya koleka.
\v 21 Yebisa bango sikawa ’te batika mosala, batonga engumba te tee ngai moko nakokata likambo.
\v 22 Botala likambo liye na bokebi mpo ’te mobulu mobima te mpe bakonzi bazwa ma­tata te. »
\v 23 Batangi maloba ma monkanda mwa mokonzi Artaksasta o miso ma moyangeli Rekum mpe mokomi mi­nkanda Simsai na baninga ba bango. Bongo bakei noki o Yeruzalem kopekisa bato ba Yuda batonga lisusu te.
\v 24 Bango batiki misala mya botongi Tempelo ya Nzambe o Yeruzalem tee o mobu mwa mibale mwa bokonzi bwa Dario, mokonzi wa Persi *.
\c 5
\s1 Batongi Tempelo lisusu (520-515)
\p
\v 1 Baprofeta Ageo na Zakaria *, mwana wa Ido, babandi kosakola o ntei ya ba-Yuda ba Yuda mpe ba Yeruzalem o nkombo ya Nzambe wa Israel oyo azalaki na bango.
\v 2 Bongo Zorobabel, mwana wa Sealtiel, na Yozue, mwana wa Yosadak, babandi kotonga lisusu Tempelo ya Nzambe o Yeruzalem. Baprofeta ba Nzambe bazalaki kopesa bango maboko.
\v 3 O mikolo mina Tatenai, moyangeli wa mokili o ngambo ya Efrate, na Setar-Bozenai na baninga ba bango bakei epai ya bango mpe batuni : « Nani ali­ngisi bino kotonga Tempelo eye mpe kobongisa lopango la yango ?
\v 4 Bato bazali kotonga ndako eye, nkombo ya bango banani ? »
\v 5 Kasi Nzambe wa bango azalaki kobatela bakolo ba ba-Yuda. Yango wana batiki mosala te, kasi batindi monkanda epai ye mokonzi Dario, mpe bazili eyano ya monkanda, bayoka bongo boniboni bakokata likambo liye.
\v 6 Tala awa maloba ma monkanda Tatenai, moyangeli wa mokili o nga­mbo ya Efrate, na Setar-Bozenai na baninga ba ye, bankumu ba mokili o ngambo ya Efrate, batindeli moko­nzi Dario.
\v 7 O monkanda bakomi boye : « Boboto mpenza na mokonzi Dario !
\v 8 Ebongi mokonzi ayeba ’te biso tokendeki kotala mokili mwa Yuda mpo tomono Tempelo ya Nzambe monene. Bazali kotonga yango na mabanga manene, batii mpe mabaya manene o bifelo bya lopango. Bazali kosala mosala na bozo mpe manso mazali kotambola malamu.
\v 9 Tosololaki na bakolo ba mboka, totuni ba­ngo : “Nani alingisi bino kotonga lisusu Tempelo eye mpe lopango la yango ?”
\v 10 Totunaki nkombo ya ba­ngo mpo ’te toyebisa yo ; bongo tokoma o monkanda nkombo ya baye bakambi misala mya bango.
\v 11 Bango bayanoli biso : “Tozali basaleli ba Nzambe wa likolo na nse. Totongi lisusu Tempelo eye etongamaka kala. Mokonzi monene moko wa Israel atongaki yango mpe asilisaki mosala mona.
\v 12 Kasi bankoko ba biso bayokisaki Nzambe wa likolo nkanda ; yango wana Nzambe akabaki bango o maboko ma Nabukodonozor, moto wa Kaldea mpe moko­nzi wa Babilon ; ye moto abukaki Tempelo mpe atindaki bato o boo­mbo o Babilon.
\v 13 Nzokande, o mobu mwa yambo mwa bokonzi bwa Siro, mokonzi wa Babilon, Siro yemei ali­ngisaki ’te batonga lisusu Tempelo eye ya Nzambe.
\v 14 Biloko binso bya wolo na palata bya Tempelo ya Nza­mbe, biye Nabukodonozor abotoloki o ndako esantu ya Yeruzalem mpo atia byango o tempelo ya ye o Babilon, mokonzi Siro alongolaki byango o tempelo ya Babilon, mpe azongisaki byango o maboko ma Sesbasar, oyo ye moko atiaki moyangeli.
\v 15 Aye­bi­saki ye : “Kamata biloko biye, zo­ngisa byango o ndako esantu ya Yeruzalem. Batonga Tempelo ya Nzambe lisu­su o esika ya kala.”
\v 16 Ntango Sesbasar akomi awa, atiaki mabanga manene ma nse ma Tempelo ya Nzambe o Yeruzalem. Banda mokolo mona tee lelo bazali se kotonga ndako, kasi mosala mosili naino te.
\v 17 Bongo, soko mokonzi alingi, baluka monkanda o ebombelo ya mosolo ye mokonzi wa Babilon, bayeba soko ya solo mokonzi Siro alingisaki kotonga lisusu Tempelo ya Nzambe o Yeruzalem. Bayebisa biso mpe eyano ya mokonzi, soko andimi to te.” »
\c 6
\p
\v 1 Na nsima mokonzi Dario atindi bato baluka monkanda o ebo­mbelo ya mosolo, esika batiaki biloko bya motuya bya Babilon.
\v 2 Mpe ya solo, o ebombamelo ezalaki o Ekbatane *, o mokili mwa ba-Medi, bazwi monkanda moko na maloba maye : « Monkanda mwa leta :
\v 3 O mobu mwa yambo mwa bokonzi bwa ye, mokonzi Siro atindi boye : Likambo lya Tempelo ya Nzambe o Yeruzalem. Batonga lisusu Tempelo mpo ezala esika ya kobonza mabonza ; babongisa mpe mabanga ma nse o esik­a ezalaka liboso. Ezala na bolai maboko ntuku motoba, na bonene maboko ntuku motoba.
\p
\v 4 Batanda milongo misato mya mabanga manene mpe molongo moko mwa mabaya ma sika. Bakofuta basali na mosolo mokouta o ndako ya mokonzi.
\v 5 Bazo­ngisa biloko binso bya wolo na palata bya Tempelo ya Nzambe, biye Nabukodonozor abotoloki o Tempelo ya Yeruzalem mpe amemeki o Babilon ; batia biloko bina binso lisusu o esika bizalaki kala o ndako esantu ya Yeruzalem.
\v 6 Bongo, bino, Tatenai, moyangeli wa mokili o ngambo ya Efrate, na Seta­r-Bozenai na bankumu banso o ngambo ya Efrate, bokoto o likambo lya bango te.
\v 7 Botika moyangeli wa Yuda na bakolo ba mboka basala mosala o Tempelo ya Yawe ; bakoki kotonga Tempelo ya Nzambe o esika ya kala.
\v 8 Tala mibeko napesi bino : Mpo ya kotonga lisusu Tempelo ya Nza­mbe, bosalisa bakolo ba ba-Yuda boye : Bofuta bango na mosolo mwa leta, ezali mpako ya bato bazali o ngambo ya Efrate. Bafuta bango ntango inso mpo ’te mosala motambola malamu.
\v 9 Bopesa bango mpe binso bisengeli na bango mpo ’te babonzela Nzambe wa likolo mabonza ma kotumba : ngo­mbe babali, mpata babali na bana mpata, mbuma ya mampa, mongwa, vino na mafuta ; bapesa byango mokolo na mokolo, se lokola banganga Nzambe ba Yeruzalem bakosenge.
\v 10 Bongo bakoka kobonzela Nzambe wa likolo mabonza ma nsolo mpi­mbo mpe kosambela mpo ya bomoi bwa mokonzi mpe bwa bana ba ye.
\v 11 Natindi mpe boye : soko moto abuki mobeko mona, balongola likonzi lya ndako ya ye, batelemisa lyango mpe babaka ye wana ; batumba ndako ya ye, ekoma esika ya kobwaka bosoto.
\v 12 Nzambe oyo atii nkombo ya ye kuna, aboma mokonzi na bato banso babuki mobeko moye mpe bameki koboma Tempelo ya Nzambe o Yeruzalem. Ngai Dario natindi bo­ngo mpe nalingi ’te batosa etinda ya ngai. »
\v 13 Tatenai, moyangeli wa mokili o ngambo ya Efrate, na Setar-Bozenai, na baninga ba bango basali manso lokola mokonzi Dario atindi bango.
\v 14 Bakolo ba ba-Yuda basali makasi mpe mosala motamboli malamu. Bato bazalaki kotala misala : Profeta Ageo na Zakaria, mwana wa Ido. Basilisi kotonga yango lokola Nzambe wa Israel atindaki, mpe engebene na mibeko mya Siro, Dario na Artaksasta, mokonzi wa Persi.
\v 15 Basilisi kotonga Tempelo o mokolo mwa misato mwa sanza Adar, o mobu mwa motoba mwa bokonzi bwa Dario *.
\v 16 Bana ba Israel, banganga Nza­mbe, ba-Levi na bato bautaki o boombo basepeli eyenga ya Bobenisi Tempelo na esengo.
\v 17 Mpo ya bobenisi Te­mpelo babonzi mpata babali zomi na ibale engebene na motango mwa mabota ma Israel.
\v 18 Bakaboli misala mya banganga Nzambe mpe mya ba-Levi, basala mosala mwa bango malamu o Tempelo ya Yeruzalem, se lokola eko­mami o buku ya Moze.
\s1 Eyenga ya Pasika
\p
\v 19 Bato bautaki o boombo basepeli eyenga ya Pasika o mokolo mwa zomi na minei mwa sanza ya yambo.
\v 20 Awa banganga Nzambe na ba-Levi bamipetolaki, banso bakomi peto ; babonzi libonza lya Pasika mpo ya baye bakangemaki o boombo, mpo ya baninga ba bango banganga Nza­mbe mpe mpo ya bango moko.
\v 21 Bongo, ba-Israel banso bautaki o boombo, balei Pasika esika yoko na baye banso batikalaki o mokili mpe baboyaki makambo mbindo ma bikolo bisusu mpo ’te bakumisa Yawe Nzambe wa Israel.
\v 22 Basepeli eyenga ya Mampa mazangi nkisi mikolo nsa­mbo, mpo Yawe atondisaki bango na esengo mpe abongisaki motema mwa mokonzi mwa Asur *, oyo asalisi ba­ngo o mosala mwa botongi Tempelo ya Yawe Nzambe wa Israel.
\c 7
\s1 Mosala mwa Esdra
\p
\v 1 Nsima ya mbula mingi, o eleko Artaksasta azalaki mokonzi wa Persi, Esdra alongwi o Babilon. Azalaki mwana wa Seraya, mwana wa Azaria, mwana wa Kilkia,
\v 2 mwana wa Salum, mwana wa Sadok, mwana wa Akitub,
\v 3 mwana wa Amaria, mwana wa Azaria, mwana wa Merayot,
\v 4 mwana wa Zekaria, mwana wa Uzi, mwana wa Buki,
\v 5 mwana wa Abisua, mwana wa Pinekas, mwana wa Eleazar, mwana wa nganga Nza­mbe mokonzi Arone.
\v 6 Esdra * oyo azalaki moteyi wa Mobeko, ayebi Mobeko mwa Moze malamu, moye Yawe Nzambe wa Israel apesaki. Mokonzi wa Persi andimeli ye o makambo manso asengeki, zambi Yawe Nzambe wa ye azalaki na ye.
\v 7 O kati ya ba-Israel, banganga Nzambe basusu, na ba-Levi, na bayembi, na bakengeli ba bizibeli bya Tempelo na basalisi ba bango bakei, elongo na Esdra, o Yeruzalem o mobu mwa nsa­mbo mwa bokonzi bwa Artaksasta.
\v 8 Esdra akomi o Yeruzalem o sanza ya itano, o mobu mwa nsambo mwa bokonzi bwa ye.
\v 9 Aponoki mokolo mwa yambo mwa sanza ya yambo mpo ya kolongwa o Babilon, mpe akomi o Yeruzalem o mokolo mwa yambo mwa sanza ya itano ; solo, Nzambe azalaki kosalisa ye.
\v 10 Esdra ayekoli Mobeko mwa Yawe na motema moko mpo ’te ameka kotosa mwa­ngo, mpe ateya ba-Israel mibeko na mimeseno mya bankoko.
\s1 Monkanda mwa Artaksasta
\p
\v 11 Tala awa maloba ma monkanda mokonzi Artaksasta apesi Esdra, nga­nga Nzambe mpe moteyi wa Mobeko oyo akomaki mitindo na mibeko miye Yawe apesi Israel.
\v 12 « Ngai Artaksasta, mokonzi wa ba­konzi banso, yamba mbote ya ngai, eye natindeli yo, Esdra, nganga Nzambe mpe moteyi wa Mobeko mwa Nzambe wa likolo.
\v 13 Natindi boye * : Ba-Israel, banganga Nzambe na ba-Levi baye balingi kozonga o Yeruzalem, bakoki kokende na yo elongo.
\v 14 Ngai mokonzi na balaki ba ngai nsambo totindi yo okende kotala soko o Yuda mpe o Yeruzalem bakosalaka lokola Mobeko mwa Nza­mbe wa yo motindi.
\v 15 Okoki kokende na palata na wolo inso ngai mokonzi na balaki ba ngai nsambo tobonzeli Nza­mbe wa Israel, oyo azali na ndako o Yeruzalem.
\v 16 Omeme mpe palata na wolo inso akoki kozwa o mokili mobimba mwa Babilon, na makabo bato ba yo na banganga Nzambe babo­nzelaki yo mpo ya Tempelo ya Nza­mbe wa bango o Yeruzalem.
\v 17 Na palata yango okoki kosomba ngombe babali, mpata babali, bana mpata mpe mabonza ma bolei mpe ma bomeli. Obonza mango o altare ya Tempelo ya Nzambe wa bino o Yeruzalem.
\v 18 Soko palata na wolo itikali, osala na yango lokola yo na bato ba yo bomoni malamu, se lokola Nzambe wa bino atindi.
\v 19 Biloko bakopesa yo mpo ya mosala mwa Tempelo ya Nzambe wa yo, tia byango liboso lya Nzambe, o Yeruzalem.
\v 20 Soko osengeli na biloko bisusu mpo ya Tempelo ya Nzambe wa yo, okoki kosomba byango na mosolo mwa ebombelo ya mokonzi.
\v 21 Ngai, mokonzi Artakasasta, napesi bakengeli ba ebombelo ya mosolo o ngambo ya Efrate etinda eye : ‘Biloko binso Esdra, nganga Nzambe na moteyi wa Mobeko mwa Nzambe wa likolo, asengi, bopesa ye byango,
\v 22 ata asengi talenta ya wolo monkama, bi­ngwongolo bya mbuma ya mampa monkama, mabenga ma vino monkama, bisangala bya mafuta monkama ; bopesa ye mpe mongwa lokola motema mwa ye molingi.
\v 23 Makambo ma­nso Nzambe wa likolo atindi masalema se bongo mpo ya Tempelo ya Nzambe wa likolo mpo ’te Nzambe ayokela ngai mokonzi na bato ba ngai nkanda te.
\v 24 Toyebisi bino mpe ’te epekisami kosenge ntako, mpako mpe ndingisa ya koleka na banganga Nzambe, ba-Levi, bayembi, bake­ngeli ba bizibeli bya Tempelo na basali basusu ba bango.’
\v 25 Yo, Esdra, na bwanya boye Nza­mbe apesi yo, pono bateyi ba Mobeko na bazuzi mpo ’te basambisa na bosembo bato ba ngambo ya Efrate, baye bayebi Mobeko mwa Nzambe wa yo. Mpe lakisa mobeko na baye bayebi mwango te.
\v 26 Moto oyo atosi Mobeko mwa Nzambe wa yo te, eye ezali mpe mobeko mwa mokonzi wa Persi, bapesa ye etumbu enene, baboma ye to babengana ye, bafutisa ye mosolo to bakanga ye o boloko. »
\s1 Esdra akei o Yeruzalem
\p
\v 27 Bakumisa Yawe *, Nzambe wa bankoko ba biso, oyo apesi moko­nzi mayele malamu ma kopesa lokumu na Tempelo ya Yawe o Yeruzalem.
\v 28 Na bolingi bwa ye asalisi ngai ’te mokonzi, balaki na bankumu ba ye bayoka ngai. Nazwi mpiko, mpo Yawe Nzambe wa ngai asalisi ngai. Nasangisi bankumu ba Israel mpo ’te bakende na ngai nzela yoko.
\c 8
\p
\v 1 Tala awa nkombo ya bankumu ba mabota balongwaki o Babilon elongo na ngai, o eleko Artaksasta azalaki mokonzi :
\v 2 Wa libota lya Pinekas : Gersom ; wa libota lya Itamar : Daniel ; wa libota lya Davidi : Atus,
\v 3 mwana wa Sekania ; ba libota lya Paros : Zekaria na babali basusu monkama na ntuku itano ;
\v 4 ba libota lya Pakat-Moab : Elionai, mwana wa Zerakia, na babali basusu nkama ibale ;
\v 5 ba libota lya Zatu : Sekania, mwana wa Yakaziel, na babali basusu nkama isato ;
\v 6 ba libota lya Adin : Ebed, mwana wa Yonatan, na babali ntuku itano ;
\v 7 ba libota lya Elam : Yesaya, mwana wa Atalia, na babali basusu ntuku motoba ;
\v 8 ba libota lya Sefatia : Zebadia, mwana wa Mikael, na babali basusu ntuku mwa­mbe ;
\v 9 ba libota lya Yoab : Obadia, mwana wa Yekiel, na babali basusu nkama ibale na zomi na mwambe ;
\v 10 ba libota lya Bani : Selomit, mwana wa Yosifia, na babali basusu monkama na ntuku motoba ;
\v 11 ba libota lya Bebai : Zekaria, mwana wa Bebai, na babali basusu ntuku ibale na mwambe ;
\v 12 ba libota lya Azgad : Yokanan, mwana wa Akatan, na babali basusu monkama na zomi ;
\v 13 ba libota lya Adonikam : bana ba nsuka, nkombo ya bango : Elifelet, Yekiel, na Semaya, na babali basusu ntuku motoba ;
\v 14 mpe ba libota lya Bigvai : Utai, mwana wa Zabud, na babali basusu ntuku nsambo.
\v 15 Nasangisi bango penepene na mo­luka, moye mokotiyolaka epai ya Akava *. Tofandi wana mikolo misato. Natali malamu, namoni banganga Nzambe na bato basusu, kasi namoni ata mo-Levi moko te.
\v 16 Yango wana natindi bakonzi Eliezer, Ariel, Semaya, Elnatan, Yarib, Elnatan mosusu, Natan, Zekaria, Mesulam na bateyi basusu Yoyarib na Elnatan,
\v 17 bake­nde epai ya Ido, mokonzi wa mboka Kasifia ; nayebisi bango bakende koyebisa Ido na bandeko ba ye bafandi o mboka Kasifia, ’te bapesa biso basalisi mpo ya Tempelo ya Nzambe wa biso.
\v 18 Awa Nzambe wa biso azalaki kosalisa biso, batindeli biso moto moko wa mayele, moko wa bana ba Makli, wa libota lya Levi, mwana wa Israel ; nkombo ya ye Serebia ; ayei na bana na bandeko ba ye zomi na mwambe.
\v 19 Ayei na Kasabia mpe na ndeko wa ye Yesaya wa libota lya Merari, mpe na bana ba ye ; banso bazalaki ntuku ibale.
\v 20 Apesi mpe basalisi basusu nkama ibale na ntuku ibale, baye Davidi na bakonzi bapesaki ba-Levi mpo ’te basalela bango. Bata­ngi bango banso moko moko.
\v 21 Kuna penepene na moluka Akava natindi ’te bato balala nzala, nalobi : Tomikitisa o miso ma Nzambe wa biso mpe tosenge ye abatela biso o mobembo, na bana na mpe biloko bya biso.
\v 22 Mpamba te, nayoki nsoni mpo ’te nasenge mokonzi apesa biso basoda ba farasa babatela biso na bayibi o nzela, zambi tolobaki na mokonzi : « Nzambe wa biso akosalisaka bato banso bakumisi ye, kasi akoyokaka nkanda enene na baye baboyi ye. »
\v 23 Yango wana tolali nzala, na ntina ena tobondeli Nzambe wa biso, mpe andimeli biso.
\v 24 O kati ya banganga Nzambe naponi bankumu zomi na babale : Serbia na Kasabia na bandeko basusu zomi.
\v 25 O maboko ma bango nameki bozito bwa palata, wolo na biloko biye mokonzi, balaki na bankumu babonzaki mpo ya Tempelo ya Nzambe wa biso, na mabonza manso bato ba Israel bapesaki.
\v 26 Napesi bango tale­nta ya palata nkama motoba na ntuku itano, biloko bya palata bya talenta monkama, talenta ya wolo monkama,
\v 27 nkeni ya wolo ntuku ibale ya motuya mwa darike nkoto yoko *, na mbeki ya mbengi ibale ingengi lokola wolo, bonzenga mpenza mpe ya motuya monene.
\v 28 Nalobi na bango : « Bino bozali babonzami na Yawe ; biloko biye bizali bisantu ; palata na wolo iye izali likabo bopesi Yawe Nza­mbe wa bankoko ba bino.
\v 29 Bokengele yango malamu mpe bobo­mba yango tee mokolo bokoki kotanga yango o miso ma banganga bakonzi, ba-Levi na bankumu ba mabota o Yeruzalem, o ndako ya ebombelo ya mosolo ya Tempelo ya Yawe. »
\v 30 Bo­ngo banganga Nzambe na ba-Levi bakamati palata na wolo na biloko batiaki o kilo, mpe bakei na yango o Yeruzalem o Tempelo ya Yawe.
\v 31 Mokolo mwa zomi na mibale mwa sanza ya yambo totiki moluka Akava mpe tokei o Yeruzalem. Nza­mbe wa biso asalisi mpe abateli biso na banguna mpe na bayibi o nzela.
\v 32 Tokomi o Yeruzalem mpe topemi mikolo misato.
\v 33 Mokolo mwa minei tomeki bozito bwa palata, wolo na biloko o Tempelo ya Yawe Nzambe wa biso, mpe totii binso o maboko ma nganga Nzambe Meremot, mwana wa Uria. Eleazar, mwana wa Pinekas, azalaki na ye. Ba-Levi Yozabad, mwana wa Yozue, na Noadia, mwana wa Binui, bazalaki mpe wana.
\v 34 Batangi motuya mpe bozito, mpe bakomi binso o monkanda.
\v 35 O mikolo mina baye bautaki o boombo babonzeli Nzambe wa Israel libonza lya kotumba ; babomi ngombe babali zomi na ibale mpo ya Israel mobimba, na mpata babali ntuku libwa na motoba, na bana mpata ntuku nsambo na ibale, na ntaba babali zomi na ibale bo moboma mwa masumu ; ezalaki libonza lya kotu­mba babonzeli Yawe.
\v 36 Na nsima bapesi monkanda mwa bitinda bya mokonzi o maboko ma bayangeli mpe ma bankumu ba mokili o ngambo ya Efrate ; bango ba-ndimi bitinda mpe basalisi bato mpe Tempelo ya Yawe.
\c 9
\s1 Losambo la Esdra
\p
\v 1 Esilisi biso kobongisa makambo mana, bakolo ba mboka bayei epai ya ngai, balobi : « Bato ba Israel, banganga Nzambe na ba-Levi baka-bwani mpenza na bato ba mokili moye te ; balandi bobele makambo mabe ma ba-Kanana, ba-Iti, ba-Perizi, ba-Ye­bus, ba-Amon, ba-Moab, ba-Ezipeti na ba-Amor.
\v 2 Bango moko na bana ba bango babalaki bilenge basi ba bikolo bina ; bato ba ekolo esantu basangani na bato ba bikolo bina. Bayangeli na bankumu bato ba liboso bakoti o mabe mana *. »
\v 3 Eyoki ngai makambo mana, napasoli bilamba mpe monkoto mwa ngai, nabendi nsuki o motó mpe ma­ndefu, nakei kofanda na mawa o mo­tema.
\v 4 Baye banso bazalaki kobanga mibeko mya Nzambe wa Israel bayei kolela epai ya ngai mpo ya mabe bato bautaki o boombo bazalaki kosala. Nafandi se wana na mawa, tee ngo­nga ya libonza lya mpo­kwa.
\v 5 O ntango ya libonza lya mpokwa, nalongwi o esika nalalaki ; elamba na monkoto bipaswani, nakwei o mabolongo, natomboli maboko epai ya Ya­we Nzambe wa ngai,
\v 6 mpe nasa­mbeli boye : « E Nzambe wa ngai, nayoki nsoni mingi mpenza mpe nabangi koto-mbola elongi epai ya yo, Nzambe wa ngai, mpo tozindi o masumu, mpe mabe ma biso masili makomi o miso ma yo.
\v 7 Banda mikolo mya bankoko ba biso tee lelo, tosali masumu mingi. Mpo ya mabe ma biso, bakabaki biso, bakonzi na banganga Nzambe ba biso, o maboko ma bakonzi ba bikolo bisusu, babomaki biso na mopanga, bakangaki biso o boombo, bayibelaki biso biloko mpe bayokisi biso nsoni tee lelo.
\v 8 Kasi sikawa, na mbala yoko, Yawe Nzambe wa biso ayokeli biso ngolu, awa abikisi mwa ndambo ya bandeko, mpe apesi biso esika ya kopema o ndako ya ye esantu. Mpe bongo, Nzambe wa biso afungoli biso miso, mpe alingisi biso topema mwa moke o boombo bwa biso.
\v 9 Tozali baombo solo, nzokande Nzambe wa biso atiki biso te. Mpo ya ngolu ya ye bakonzi ba Persi bandimeli biso, balingisi biso totonga lisusu Te­mpe­lo ya Nzambe wa biso, tobongisa engumba mpe tokanga mapango ma biso o Yuda mpe o Yeruzalem.
\v 10 Kasi sikawa, e Nzambe wa biso, toloba nini lisusu nsima ya makambo mana ? Totiki bitinda bya yo,
\v 11 biye basaleli ba yo baprofeta bapesaki biso na maloba maye : ‘Mokili esika bokoyingela mpo ’te bokoma bankolo mobebi na mbindo ya bato bakosalaka makambo ma nsomo bipai binso.
\v 12 Yango wana bobalisa bilenge basi ba bino te na bilenge babali ba bango. Bokamata bilenge basi ba bango te mpo ya bana babali ba bino. Soko boluki bondeko mpe boboto na ba­ngo te, bokokoma makasi mpe bokolia mbuma ya mokili mwa bango, mpe bana ba bino bakofanda kuna mikolo minso.’
\v 13 Ya solo, toyokaki mpasi mpo ya mabe tosalaki na bozoba bwa biso. Kasi yo, Nzambe wa biso, opesi biso bobele mwa etumbu, mpe olingaki ’te ndambo ya bato batikala.
\v 14 Mpo nini toboya bitinda bya yo, mpe boniboni toluka lisusu kokangana na bato baye babe na libala ? Soko tosali bongo, okoki koyokela biso nkanda mpe koboma biso banso tee ata mwa nda­mbo ya bato etikala lisusu te.
\v 15 E Yawe Nzambe wa Israel, yo ozali na bosembo, zambi totikali naino mwa ndambo ya bato babiki o mokolo mwa lelo. Tala biso baye, tozali o boso bwa yo na masumu ma biso ; solo, o miso ma yo moto moko akoki kobika te. »
\c 10
\s1 Balongoli basi bampaya
\p
\v 1 O ntango Esdra azalaki kolela bongo mpe kokumbama o boso bwa Tempelo ya Nzambe mpo ’te asambela mpe asakola masumu ma ye, bato baike ba Israel, babali na basi na bana, bayei epai ya ye mpe baleli makasi.
\v 2 Sekania, mwana wa Yekiel, nkoko wa Elam, alobi na Esdra : « Tosali masumu o miso ma Nzambe wa biso, awa tobalaki basi bampaya ba mokili moye. Kasi ata tosali bo­ngo, Israel azali na mwa elikya.
\v 3 Sikawa tokolaya ndai o miso ma Nza­mbe wa biso ’te tokolongola basi ba biso bampaya, na bana ba bango ; tokolanda bobele lilako lya yo, moko­nzi wa biso, na lilako lya bato banso bakotosaka mobeko mwa Nzambe wa biso. Esalema se lokola Mobeko motindi !
\v 4 Yo teleme, likambo liye litali yo ; biso tokosalisa yo ; banda mosala mpe sala makasi. »
\v 5 Esdra atelemi mpe alayisi bakonzi ba banganga Nzambe na ba-Levi na bato banso ba Israel ndai ’te bakosala lokola balobaki ; mpe banso balayi ndai.
\v 6 Esdra atiki esika afandaki o boso bwa Tempelo ya Nzambe mpe akei o ndako ya Yekonan, mwana wa Eliasib. Alali wana na butu, alei ma­mpa te, ameli mpe mai te, mpo azalaki na mawa manene mpo ya masumu ma bato bautaki o boombo.
\v 7 Basakoli likambo liye bipai binso o Yuda mpe o Yeruzalem, bayebisi bato banso bautaki o boombo ’te baya kosangana o Yeruzalem.
\v 8 Moto oyo akoya te o kati ya mikolo misato mikoya, mpe akolanda etinda ya ba­nkumu mpe ya bakolo te, babotolo ye biloko binso mpe balongola ye o lisanga lya bato bautaki o boombo.
\v 9 Bongo bato banso ba Yuda mpe ba Benyamin bakei kosangana o Yeruzalem o mikolo misato mina. Ezalaki o sanza ya libwa *, mokolo mwa ntuku ibale. Bato banso bakei koteleme o libanda o boso bwa Tempelo ya Nza­mbe, balengi mpo ya likambo lina mpe mpo ya mbula makasi.
\v 10 Nganga Nzambe Esdra alobi : « Bosalaki mabe, awa bobalaki basi bampaya, bobakisi lisumu likolo lya masumu manso ma Israel.
\v 11 Sikawa bosakola makambo ma bino o miso ma Yawe Nzambe wa bankoko ba bino, mpe bolanda nzela ya ye ; boka-bwana na bato ba mokili mona mpe bolongola basi bampaya. »
\v 12 Bato ba­nso bayanoli na mongongo makasi : « Solo, tondimi, tokolanda maloba ma yo !
\v 13 Kasi bato bazali mingi mpe sikawa ezali eleko ya mbula, tokoki kotikala awa o libanda te ; ezali lika­mbo lya mokolo moko to mikolo mibale te ; mpamba te, bato basali mabe mana bazali mingi.
\v 14 Malamu bako­nzi ba biso * bazala wana o nkombo ya lisanga, mpe baye banso bazali o bingumba bya biso mpe babalaki basi bampaya, baya o mokolo toyokani elongo na bakolo ba mboka mpe na bazuzi ba bango tee mokolo Nzambe wa biso akokitisa motema mpo ya likambo lina. »
\v 15 Bobele Yonatan, mwana wa Asael, na Yezaya, mwana wa Tikva, ba-ndimi te ndenge bokati likambo * ; Mesulam na mo-Levi Sabtai bandimi mpe te.
\v 16 Kasi bato bautaki o boo­mbo basali lokola batindaki. Nganga Nzambe Esdra aponi bakolo ba mabota ma batata ba bango, batangi ba­ngo na nkombo. Babandi mosala mwa bokati makambo o mokolo mwa ya­mbo mwa sanza ya zomi.
\v 17 Basilisi kokata makambo ma babali banso babalaki basi bampaya o mokolo mwa yambo mwa sanza ya yambo.
\s1 Nkombo ya bato basali mabe
\p
\v 18 O kati ya banganga Nzambe babalaki basi bampaya, bazwi bato baye : O libota lya Yozue, bazwi mwana wa Yosadak na bandeko ba ye : Maseya, Eliezer, Yarib na Gedalia ;
\p
\v 19 bango balayi ndai ya kolongola basi ba ba­ngo mpe ya kobonzela mpata mobali yoko mpo ya bofuti nyongo ya masumu.
\v 20 O libota lya Imer bazwi Kanani na Zebadia.
\v 21 O libota lya Karim, bazwi Maseya, Elia, Semaya, Yekiel na Uzia.
\v 22 O libota lya Pasekur, bazwi Elioenai, Maseya, Yismael, Netanel, Yozabad na Eleasa.
\v 23 O libota lya ba-Levi, bazwi Yozabad, Simei, Kelaya (to Kelita), Petakia, Yeyuda na Eliezer.
\v 24 O libota lya bayembi, bazwi Eliasib na Zakur. O libota lya bakengeli ba bizibeli bya Tempelo bazwi, Salum, Telem na Uri.
\v 25 Mpe o kati ya ba-Israel ; o libota lya Paros bazwi Ramia, Yiziya, Malia, Miyamin, Eleazar, Malkia na Benaya.
\v 26 O libota lya Elam bazwi Matania, Zekaria, Yekiel, Abdi, Yeremot na Eli­kya.
\v 27 O libota lya Zatu bazwi Elioenai, Eliasib, Matania, Yeremot, Zabad na Aziza.
\v 28 O libota lya Bebai bazwi Yokanan, Kanania, Zabai na Atlai.
\v 29 O libota lya Bigvai bazwi Mesulam, Maluk, Yedaya, Yasub, Yisal na Yeremot.
\v 30 O libota lya Pakat-Moab bazwi Adna, Kelal, Benaya, Maseya, Matania, Besalel, Binui na Manase.
\v 31 O libota lya Karim bazwi Eliezer, Yisia, Malkia, Semaya, Simeon,
\v 32 Bi­nyamin, Maluk na Semaria.
\v 33 O libota lya Kasum bazwi Matenai, Matatia, Zabad, Elifelet, Yeremai, Manase na Simei.
\v 34 O libota lya Bani bazwi Madai, Amram, Yoel,
\v 35 Benaya, Bedia, Kelaya,
\v 36 Vania, Meremot, Eliasib,
\v 37 Matania, Matenai na Yasai.
\v 38 O libota lya Binui bazwi Simei,
\v 39 Selemia, Natan na Adaya.
\v 40 O libota lya Zakai bazwi Sasai, Sarai,
\v 41 Azareel, Selemia, Semaria,
\v 42 Salum, Amaria na Yozefu.
\v 43 O libota lya Nebo bazwi Yekiel, Matitia, Zabad, Zebina, Yadai, Yoel na Benaya.
\v 44 Bango banso babalaki basi ba­mpa­ya, balongoli ná basi ná bana.
