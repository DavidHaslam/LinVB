# LinVB

[Lingala](https://en.wikipedia.org/wiki/Lingala) traduction [**Verbum Bible**](http://verbumbible.org/) 2009, with SWORD module créé par fr Cyrille/[L'Africain](https://gitlab.com/lafricain79).

### Folders:
- scripts &ndash; contains the bash scripts used to convert the source file data
- usfm  &ndash; contains the USFM files for 73 books plus the Bible introduction 
- notes &ndash; contains the text for the footnotes
- osis  &ndash; contains the OSIS XML file used as the input for osis2mod
- textpipe &ndash; contains a TextPipe filter [David](https://gitlab.com/DavidHaslam) used to postprocess the XML file
- sword &ndash; contains the SWORD module complete with configuration file
- analysis &ndash; contains output files from various analysis tools
- Excel &ndash; contains a worksheet covering Lingala book names
- zip &ndash; contains a compressed copy of the SWORD module
- v11n &ndash; contains proposed new alternative versification schemes

DistributionLicense=Copyrighted; Permission to distribute granted to [CrossWire](http://crosswire.org/)

The USFM files are maintained by L'Africain using [Bibledit-Desktop](https://github.com/LAfricain/bibledit-desktop) 

Conversion of USFM files to OSIS XML was done using the Python script [u2o.py](https://github.com/adyeths/u2o)

## Status

- Introduction and Bible text - DONE
- Footnotes to be formatted and inserted
- Cross-references to be converted
- Module to be submitted for release by CrossWire


