1,5. Mokolo bakomi bakristu na bokuli batisimo.
1,6. O nsuka ya molóngó.
1,12. Bakangaki Polo mpe bazalaki kosambisa ye.
2,9. Nkombo eleki nkombo inso : Mokonzi (Kurios) : nkombo eye epesamaki na Nzambe (Yawe) o Bondeko bwa Kala.
2,10. Tala Iz 45,23.
2,16. Maloba ma Nsango Elamu maye makopesaka bomoi.
2,17. Polo ayebi naino te soko akobima o boloko.
2,25. Epafrodito : Atangemi bobele awa mpe o 4,18. Ye moto ayelaki Polo makabo ma ba-Filipi ; mbele amemeleki ba-Filipi monkanda moye ntango azongi.
2,30. Polo alakisi awa mosala mwa kosangela Nsango Elamu ya Yezu.
3,1. Maloba maye malakisi ’te Polo alingi asilisa monkanda mwa ye. Yango wana bato basusu bakanisi ’te eteni elandi awa (3,1-4,1) ezali monkanda mosusu mwa Polo, eye babakisi awa.
3,2. Basali babe : bateyi ba lokuta ; Polo abengi bango ‘mbwa’. Na nkombo eye ba-Yuda bazalaki kobenga bapagano (baye bazali ba-Yuda te), baye bakatisi nzoto te (tala Mt 15,26).
4,3. –Polo akokaki kotia motema na bandeko baye babale ba solo mpo ya koyangela lisanga lya bakristu.
–O buku ya bomoi : Tala Boy 3,5.
4,15. Polo azalaki koyamba mabonza ma bakristu te, bobele awa ayambi mango o maboko ma bakristu ba Filipi. Tala Bik 17,1... ; 18,3 ; 1 Kor 4,12 ; 9, 12-15.
4,21. Basantu : lokola ’te bandeko. Tala Rom 1,7+.