1,1. –Tala Rom 1,7+.
–Bandeko : ekomami ‘basantu’.
1,22. –Tala Nz 8,7.
–Eklezya : O minkanda ona ba-Efeze mpe ona ba-Kolose Polo alakisi Ekle­zya mobimba, bobele Eklezya ya mboka iye te.
2,6. Uta sikawa tofandi o likolo, mpo tozali na Kristu nzoto yoko mpe Kristu afandi o loboko la mobali la Tata.
2,15. Moto wa sika : Kristu oyo azali motó mwa nzoto ya ye Eklezya. Ye moto alongoli mobeko mwa kala mozalaki kopekisa ba-Yuda kosangana na bapagano esika yoko ; banso bakomi libota lyoko lya bana ba Nzambe.
2,20. Baprofeta ba Bondeko bwa Sika. Tala mpe 3,5 ; 4,11 ; 1 Kor 12,10.
3,1. –Polo akangemi o boloko o Roma (Bik 28).
–Polo asilisi molongo moye te ; akosilisa mwango o 3,14.
3,3. Limoni amonoki mokolo akutanaki na Kristu o nzela ya Damasko. Tala 2 Kor 12,1 ; Gal 1,16 ; Bik 9,15 ; 22,21 ; 26, 16-18.
4,1. Polo akangemi o boloko o Roma.
4,7. Ngrasya : Makabo ndenge na ndenge ma Elimo Santu (tala 1 Kor 12, 1).
4,10. Bakomi : atondisa biloko binso.
4,12. Ekomami ‘basantu’.
4,22. Oyo abongoli naino ezaleli te mpo alanda Kristu na motema moko.
4,30. Elimo Santu azali nkoma o milimo mpo ’te bayeba bato ba Kristu o Mokolo Mokonzi akoya kokamata biso. Ye ndanga ya libiki (1,14), likabo lya yambo. Libiki likozala mobimba bobele mokolo Kristu akozonga.
5,14. Mbele maloba maye mazali maloba ma nzembo bakristu ba yambo bazalaki koyemba ntango ya batisimo.
5,26. Ma batisimo : Mbakisa ya biso mpo ya kolimbola.
5,32. Mobombamo monene : Ntina enene mpenza ya libala, lokola tokoki kotanga o Lib. Polo akokokisa yango na Libala lya Kristu na Eklezya ya ye ; libala lya bakristu lizali lokola elili ya mobombamo moye.
6,18. Bandeko : ekomami ‘basantu’.