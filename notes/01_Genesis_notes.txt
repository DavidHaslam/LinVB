1,5. Moto akomi buku eye alingi kopesa ndimbola ya ebandela ya molóngó mobimba lokola bato ba mayele ba sikawa te. Buku eye ekomami na ntina ena te. Alingi bobele koyebisa ’te Nzambe akeli manso tokoki komono. Yango wana akaboli mango lokola azalaki komono ­ma­ngo o likolo, o mbu, o mokili, epai na epai. Alakisi bongo Nzambe lokola ­moto asali mikolo motoba, o mokolo moko moko mosala mosusu, mpo apema o mokolo mwa nsambo (Sabato), engebene na mobeko Nzambe apesaki ba-Israel. Kasi toyebi na bosolo ’te Nzambe akeli binso lokola mosali te, kasi se na ndinga ya ye : ekoki na Ye ’te akanisa byango mpo bizala.
1,7. Ba-Israel, na bato banso ba eleko ena, bakanisaki ’te likolo lya mokili ezipeli enene ezalaki eye ekaboli nse eye na mai manene mazalaki o likolo.
1,16. Mokomi alingi koteya ’te mwese na sanza izali bobele bikelamo. Mpo bato ba bikolo bazalaki kotala ‘miinda minene miye mibale’ lokola banzambe, atangi yango na nkombo ya myango te.
1,26. ‘Moto’ : o ebrei ekomami ‘Adamu’, lokola ’te moto nyonso.
1,27. Na maloba maye ‘elili ya ye’ ali­ngi kolakisa ’te bobele moto, nyama te, akoki kosolola na Nzambe. Elakisi mpe ’te Nzambe akeli mwasi na mobali na lokumu se loko, ata nzoto ya bango ikeseni.
1,29. Bato na nyama balia bobele mbuma, misuni te : elakisi ’te o libandela makambo makasi makasi na bitumba bizalaki naino te ; binso bisalaki se na boboto.
2,2. Na bolakisi ’te Nzambe apemi o mokolo mwa nsambo (Sabato), balingi koteya bato ’te basengeli kosala se bo­ngo. Mobeko moye mwa bopemi mobimaki bobele o ntango ya Mose (Bob 20,8-11).
2,4. Lisolo lya mabale lya bokeli bwa Nzambe libandi awa. Se lokola o lisolo lya yambo, awa mpe mokomi alingi bobele kolakisa ’te bikelamo binso biuti se na Nzambe.
2,7. Liloba lya ebrei ‘Adamu’ (lilakisi ‘moto’) liulani mingi na ‘adamah’ : elakisi mabelé o ebrei. Mbele mokomi alingi kolakisa na yango ’te moto azali se motau lokola mbeki ya mabelé : asengeli kokata bilanga mpe akozonga o mabelé.
2,8. Mbele nkombo Eden elakisi se esika ya elengi ya kofanda.
2,9. Ezali nzete ya solo te, ezali se elili : elakisi ’te bomoi bonso mpe bwanya bwa solo bikoutaka se na Nzambe. Bobele ye akoki kolakisa nini ezali malamu mpe nini ezali mabe. Moto akoki kobo­ngola yango te. Bongo ‘kolia mbuma epekisami’ elakisi moto aboyi kotosa Mozalisi wa ye, oyo alingi se bolamu bwa ye.
2,18. Soko moto atikali ye moko, mosika te akopusa komikanisa. Soko mwasi na mobali babalani, ekozala bobele mpo ya kobota bana te, kasi mpo ya kokolisana, moto na moto engebene na ndenge Nzambe akabeli ye : na nkita ya motema mpe na mbuma ya mosala mwa ye. Mwasi azali mosaleli wa mobali te, kasi moninga wa bolingo. Ata libala lizali eloko elamu mpenza, bato basusu bako-byangemaka mpo ya komipesa mobimba na Nzambe mpe mpo ya kosalela bani­nga, lokola Kristumei alakisi biso.
2,23. Mpo ya kolakisa ’te lisanga lya mobali na mwasi lileki lisanga lyonso, ba-Ebrei bazalaki koloba ‘monkuwa mwa minkuwa mya ngai’. O bikolo mi­ngi bya Afrika bakolobaka ‘libumu lyoko’ to ‘makila moko’.
3,1. Moto nyonso akobangaka nyoka mpe akokimaka yango lokola nyama ya likama mpenza. Awa nyoka elakisi lise-nginya linene likoteli moto o motema. O bisika bisusu (Yob 11,6-12 mpe Bw 2,24) elakisi monguna wa Nzambe mpe wa bato.
3,7. Liboso lya kosala lisumu mwasi na mobali bazalaki na boyokani bozangi ntembe.
3,15. Banda mokolo moto asali lisumu etumba ebandi kati ya bolamu na bobe, o mokili mpe o motema mwa moto nyonso. Nzokande na nsuka bobe bokolonga te, mpo Nzambe alakeli bato libiki lya seko.
3,16. Mpasi inso o mokili iuti na lika­mbo lya moto aapi kotosa Nzambe. ­Ya­ngo wana kobota na mpe mosala mo­nso mokomi mpasi. Mpasi enene ekokwelaka mpe bato banso : basengeli kokufa.
3,20. O ebrei nkombo ‘Eva’ elakisi ‘mama wa bato banso’.
3,24. Awa moto aboyi kondima bokeseni kati ya bolamu na bobe, lokola Nza­mbe alakisaki ye, akozala na bomoi bwa solo lisusu te. Na nguya ya ye moko akokoka kobongisa lisusu boyokani kati ya ye na Nzambe te, kati ya mwasi na mobali mpe te, kasi bobele na lisalisi lya Nzambe.
4,5. Nzambe akebi na libonza lya Kain te, mpo motema mwa ye mobongi te.
4,19. Nzambe akeli moto mobali na mwasi. Moto akamati basi babale akokinolaka mokano mwa Nzambe. Likambo lya­ngo likoki se kokolisa masumu o mokili.
5,29. O ebrei nkombo eye elakisi ‘oyo akobondoko’ to ‘oyo akopesaka bopemi’.
6,17. O bikolo mingi bato babosanaki te ndenge, o mibu mya kala, mai ma bibale Tigris na Efrate mabutaki mingi mpe­nza, mazindisaki bingumba na mboka ya penepene, mpe mabomaki bato ebele.
7,2. Ba-Israel batalaki nyama isusu lokola nyama mbindo, isusu lokola nyama peto, iye bakoki kolia mpe kobonza. Yango wana Nzambe atindi Nowe ’te akamata mwasi na mobali moko ya nyama peto inso, bobele mpo ya kolia te, ­kasi mpe mpo ’te akoka kobonzela Nza-mbe yango nsima ya mpela enene.
8,4. Ngomba o eteni ya Nordi ya Mezopotamia, o ekolo bakobengaka sikawa Aromenia (Turkia).
8,20. O altare, eye ezalaki lokola mopiko mwa mabanga, bazalaki kotumba mabo­nza.
9,13. O bikolo bisusu bazalaki koloba ’te na monama, moye mokomononoko o liko­lo nsima ya mbula, Nzambe autaki kobete makula (minkalali) mpo ya kotu­mbola bato. Nsima ya mbula emononoki lokola elembo ’te Nzambe akomi lisusu na motema boboto, mpo atiki kopesa etumbu.
10,1. Molongo moye molakisi nda­mbo ya bankoko baye bato bayebaki naino. Na bolakisi Nowe lokola nkoko wa bato banso, mokomi alingi koteya ’te bato banso basengeli koyokana bo ba­ndeko.
11,1. O Mezopotamia ngomba isanda izalaki te esika bato bakokaki kobuta mpo ya kokumbamela banzambe ba bango. Yango wana bato ba Babilon batongaki linongi liye liyebanaki mingi. Nsima ya mwa mibu libandaki kobukana : mokomi otali yango lokola etumbu ya Nzambe mpo ya lolendo la bato.
11,10. Bakomi ’te bankoko baumelaki mibu mingi boye na bomoi, mpo ya koteya ’te batangi basengeli kopesa bango lokumu lonene.
12,1. Nzambe asengi Abarama ’te atika ekolo eye abotami mpo akende o Kanana, kasi alakeli ye ’te akobenisa ye mpe akopesa ye bankoko ebele, baye akopesa mpe bobenisi bwa ye.
12,15. Bakonzi ba Ezipeti bazalaki komipesa nkombo eye ya lokumu (Farao), lokola ’te ‘ndako enene’, to nkolo wa ndako eye.
14,3. Mbu ya Mongwa, etangemaki mpe ‘Mbu ya Liwa’. Mpo ya mongwa mazali mingi o mai ma yango, mbisi ata yoko te ekoki kozala wana.
16,11. Nkombo Ismael lokola ’te ‘oyo Nzambe andimeli’. Tala mpe Lib 17,20 ; 21,17.
17,5. Soko ba-Israel bapesi moto nko­mbo ya sika, elakisi ’te bayebisi ye bongo ’te akozwa bolamu. Abram elakisi ‘tata wa ye azwi lokumu’, Abarama ‘tata wa bato ebele’.
17,10. Kokata nzoto ezalaki momeseno o bikolo mingi. Epai ya ba-Israel ­ezalaki mpe elembo tee o nzoto ya ba­ngo ’te bandimi Bondeko boye Nzambe akati na bango.
17,15. Nkombo iye ibale Sarai na Sara lokola ’te « mwana mwasi wa mokonzi », kasi awa elakisi ’te akokoma mama wa bakonzi mpe wa bikolo.
17,19. Nkombo Izaka lokola ’te ‘oyo aseki’. Awa (17,17) ezalaki tata moto aseki. Na nsima (18,12-15) babengi mwana Izaka, mpo mama aseki. O 21,6 bato ba­nso bayoki nsango ya mbotama ya mwana baseki elongo na Sara.
19,25. Lelo bakanisi ’te Sodoma na Gomora ikufaki mpo ya boningani bwa mokili. Na nsima mabelé mazindaki, mpe mai ma Mbu ya Liwa moyaki kozipa esika yango.
19,38. Ben-Ami lokola ’te ‘mwana wa ekolo ya ngai’. Na lisolo liye (lya bandeko basi babale) ba-Israel bazalaki kotiola bikolo bibale bya penepene, ba-Moab na ba-Amon, baye bayaki kobundisa bango mbala mingi.
20,12. O eleko liboso lya Moze epekisamaki te kobala moto abotami na tata moko mpe na mama mosusu. O Lv 18,9 tokotanga ’te ekomi ekila.
21,28-31. Na lisolo liye balingi koli-mbola ntina ibale ya nkombo Berseba : ‘mpata nsambo’ mpe « libulu lya ndai’.
22,1-19. Nzambe ameki botosi bwa Abarama na ndenge ya makasi mpenza : asengi ye ’te abonzela ye mwana, se lokola bato basusu bazalaki kosala o eleko ena. Kasi na nsuka tomoni ’te Nzambe akoki kosepela na libonza lya bana te, 
andimi se mabonza ma nyama.
23,15. Ezalaki motuya monene mpe-nza, kilo soki itano ya palata. O eleko ena bazalaki na bibende bya mosolo naino te, bazalaki komeka bosito bwa wolo to bwa palata.
23,19. Abarama mpe akokundama o esikamei akundi mwasi wa ye (25,9-10) ; Izaka, Rebeka, Yakob na Lea bakoku-ndama mpe wana (49,31).
24,30. Laban azalaki na mposa ya mosolo. Ata amilakisi lokola moto ayebi koyamba bampaya, asili aluli biloko biye akoki kozwa.
24,53. Ezali mosolo mwa libala.
25,8. O mboka ya bawa.
25,31. Mwana wa liboso azalaki kozwa bobenisi bwa tata mpe biloko bileki mbala ibale biye mwana wa nsima azalaki kozwa. Kasi azalaki na lotomo la ­kosalisa bandeko ba ye ntango bakomi kokelela.
26,12. Moke moke bato ba-Israel batiki mosala mwa kobokolo bibwele, mpe bakomi kokata bilanga.
27,1-45. Mpo ya likambo liye lya bokosi, Yakob asengeli kokende kobombama mos­ika na ndeko wa ye. Nzokande Nza­mbe akoponoko oyo ye alingi : Nzambe akokiseli ye elako eye alekalaki Abarama.
27,33. O ntango ena bakanisaki ’te maloba ma bobenisi moto to bwa botombeli ye bobe makokaki kobongwana lisusu te.
27,45. Engebene na mibeko mya leta bakatelaki mobomi nyonso etumbu ya liwa. Yango wana Rebeka ayebi malamu ’te akobungisa bana ba ye babale soko Ezau abomi Yakob.
29,14. Tala 2,23+.
29,25. Yakob asengelaki kokundola o motema ndenge akosaki ná tata ná ndeko wa ye na mbala yoko. Mbele andimaki bongo kosalela bokilo wa ye mibu nsa-mbo bo etumbu.
30,14. Bato bakanisaki ’te mbuma eye ya zamba ezalaki kosalisa basi mpo bakoka kobota. Yango wana Rakele alingaki kolia yango.
31,19. Abarama, Izaka na Yakob balakelaki Nzambe ’te bakokumbamela se Ye, kasi Laban na bana ba ye bazalaki naino na boyambi boye bwa solo te. Yango wana bazalaki na bikeko bya ba­nzambe o ndako.
32,4. Edom ezali nkombo esusu ya Ezau na mpe nkombo esusu ya ekolo ya Seir, o Sudi ya Mbu ya Liwa.
32,29. Nkombo Israel lokola ’te ‘Nzambe alakisi bokasi bwa Ye’. Kasi ndenge nini alongaki Nzambe ? Bobele mpo azwaki maye alingaki : bobenisi bwa Nzambe.
33,17. Ata azwi bolimbisi bwa ndeko wa ye, Yakob atikali se motema likolo, mpe alandi nzela esusu.
34,14. Bana ba Yakob balobi bongo na mitema mibale. Ya solo, bakoki koyamba mompaya o libota lya bango te soko ­liboso bakatisi ye te, kasi balobi yango na bokosi, mpo bazalaki na mayele ma koboma bango.
35,2. Yakob alingi ’te bato banso ba libota lya ye bakumbamela bobele Nza-mbe se moko. Yango wana atindi ’te balongola bikeko bya banzambe basusu. Tala mpe 31,19+.
37,3. Ezalaki elamba ya lokumu, eye elakisi ’te Yakob apusi kolinga Yosefu. Yango nde eyokisi bandeko ba ye zuwa.
37,21. Awa balobi ’te Ruben alingi kobikisa ndeko wa ye. Kasi na nsima (37,26) bakomi ’te ezalaki Yuda. Se bo­ngo na bato basombi Yosefu : o 37,25 ­bakomi ’te ezalaki ba-Ismael, o 37,28 ekomami ’te ezalaki ba-Madian. Tobosana te ’te batata bazalaki kosolola yango na bana, bana baye lisusu na bana ba bango, tee tee... ekomami o minkanda bobele mibu mingi na nsima.
38,8. Ba-Yuda bazalaki na mobeko motindi ’te soko mobali awei kasi aboti te, ndeko wa ye asengeli kobala mwasi oyo. Soko baboti mwana, batali ye lokola mwana wa ndeko mowei.
38,24. Tala mobeko mokomami o Lv 21,9 mpe o Mit 22,23.
41,42. Mpete eye ezalaki na elembo ya Farao ; Yosefu azwi bongo bokonzi bokokani na bokonzi bwa Farao.
42,7. Yosefu alingi kozongisela bango mabe te, maye basalaki ye ; alingi bobele koyeba soko babongoli mitema, yango wana akomeka bango.
45,5. Tala liteya lileki mateya manso o lisolo liye : Nzambe moto akambi maka­mbo manso ; ata bato babebisi na mabe ­basali, Nzambe akoki kosala ’te manso masukela bango malamu. Tala 45,7 mpe 50,10.
45,28. Mobange Yakob boyangani na lokumu mpe na nkita ya mwana wa ye Yosefu te. Azalaki se na mposa ya komono ye lisusu liboso ’te akufa.
46,8. O botangi molongo mwa mabota ba-Yuda bameseneki kotanga se babali. Soko batangi Dina, ezali se mpo ya likambo likwelaki ye (34,1-4). Toyebi te ntina nini batangi mpe Sera awa.
48,17. Nzokande Manase azalaki mwana wa liboso ; ye moto asengeli ko­zwa bobenisi bwa tata wa ye. Na nsima, mpo libota lya Efraim lisalaki na ntina leka liye lya Manase, balobaki ’te Yakob apesi bango bobenisi bwa ye.
49,1. Yakob ayebisi bana banso maye makokwela mabota ma bango. Maloba masusu awa mazali polele te, mpe toyebi makambo manso te maye makokwela bango.
49,4. Ruben alalaki na mwasi wa babale wa tata wa ye (35,22).
49,5. Simeon na Levi bakoswa bobenisi bwa tata te, mpo basalaki ba-Sikem mabe mingi (34.25-31).
49,8. Soko Yakob alobi makambo mingi mpo ya libota lya Yuda, ezali mpamba te : bokonzi bwa Sudi bokozala se na bato ba libota liye. Bato basusu bakanisi ’te makambo masusu Yakob asakoli awa matali boyei bwa Mobikisi, Mokonzi wa biso Yezu Kristu.
49,28. Mabota ma Israel mazali zomi na mabale. Nzokande toyebi ’te libota lyoko lya Yosefu, mpo lileki linene, likokabwana na mabota mabale. Libota lya Simeon, liye likofanda o Sudi ya Kanana, likolimwa. Se bongo na ba-Ruben bakokende kofanda o Esti ya Mbu ya Liwa.
50,20. O nsuka ya lisolo liye lya ­Yosefu, mokomi azongeli naino mateya manene ma lyango mabale : tokoka kolimbisa na motema moko mabe maye basali biso ; tokoka mpe kotia mitema na bolamu bwa Nzambe, oyo akokambaka ­makambo ma biso manso ; ata mabe ­maye makweli biso, Nzambe akosalaka ’te masukela biso malamu.