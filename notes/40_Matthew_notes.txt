1,1. Mateo alingi kolakisa ’te Yezu Mesiya azali nkoko wa Abarama (Lib 12,3) mpe mwana wa Davidi (2 Sam 7, 13-16), lokola Nzambe alakelaka ba-Yuda. Atangi bankoko banso te. Mbala isato atii bankoko zomi na banei mpo ’te bato bakoka kokundola bango o mitema.
1,16. Kristu, lokola ’te ‘oyo Nzambe apakoli mafuta’. Kala, ntango moto a­zwi bokonzi, bapakoli ye mafuta masantu. Se bongo na oyo baluboli nganga mokonzi. Kasi Yezu aleki bango banso na lokumu, zambi Nzambe aponi ye bo mobikisi wa bato banso.
1,18. Abandemaki na : o miso ma ba-Yuda moto asili kofuta mbongo ya libala amononi lokola moto wa libala mpenza, ata bayingisi mwasi bobele na nsima o ndako ya ye.
1,19. Yozefu atangemi moto semba : alingi te ’te basala na Maria lokola ekomami o mobeko (Mbk 22,20-21), mpo ayebi ’te likambo liye liuti na Elimo Santu.
1,21. Nkombo Yezu, lokola ’te ‘Yawe (Nzambe) akobikisaka’.
1,25. Maria aboti Yezu na bongondo na nguya ya Elimo Santu ; atikali mpe ngo­ndo nsima.
2,1. Banganga minzoto, bakotangaka bango mpe ‘bakonzi’. Bazalaki bapagano. Nzambe alingaki ’te bapagano baya kokumisa Mwana wa ye : Yezu ayei kobikisa ba-Yuda na bapagano lokola.
2,2. Mbele Mateo akanisi awa maloba ma Balame makomami o Mit 24,17 : monzoto mouti o libota lya Yakob mokomi mokonzi.
2,4. Bateyi mingi ba Mobeko bazalaki ba-Farizeo, kasi banso te.
2,15. Engebene na maloba ma profeta Ozea, esika alakisaki ekolo ya Israel lokola Mesiya (Oz 11,1).
2,18. Tala Yer 31,15 : Rakele, mama nkoko wa ba-Yuda, aleli bana ba ye bakangemi o boombo o ekolo ya ba-Asur ; akundami o mboka Beteleme.
2,23. Nazarete : mboka eke : Mesiya akobotama lokola moto monene te, engebene na maloba ma baprofeta (Iz 11,1).
3,2. Bokonzi bwa Likolo bokomi pe­nepene : Makambo manso makobo­ngwana, makokoma ma sika ! Nzambe akoyangela binso lokola Mokonzi ! Mpo ya Kristu, Mwana wa ye, oyo akokweisa zabolo, akolongola masumu, akolonga liwa ! Bato babongola mitema, bayamba Yezu, basangana na ye o mosala mwa kolendisa Bokonzi bwa Nzambe ; bongo bakosangana mpe na ye o bomoi bwa nkembo ! O Nsango Ndamu isusu bakomi ‘bokonzi bwa Nzambe’, kasi Mateo alandi momeseno mwa ba-Yuda baye bazalaki kotanga nkombo ya Nzambe te, mpo ya limemya. Yango wana bazalaki kotanga ye na nkombo isusu (Nta-Bokasi, Nta-Likolo).
3,4. Mbele Yoane amonoki mapalela ata mokolo moko te. Liloba ‘akrides’ o greke elakisi bobele ‘mapalela’ te (lokola babongolaki mbala mingi), elakisi mpe ‘mitó mya bitekuteku’. Yango wana tobongoli yango bongo.
3,7. Ba-Farizeo bazalaki koloba ’te bakotosaka Mobeko mwa Moze mobimba. 
Basusu bazalaki bato ba bosembo, kasi mingi bazalaki bato ba lolendo. 
Ba-Saduseo : bazalaki kondima bobele buku itano ya yambo ya Biblia, nzoka­nde bazalaki kotosa mitindo mya Nzambe malamu mpenza te ; bazalaki mpe kondima bokonzi bwa ba-Roma. Banganga Nzambe mingi bazalaki o molongo mwa bango.
Ba-Yuda bakanisaki ’te Nzambe akoya kobanda Bokonzi bwa ye na bopesi bitumbu na basumuki baye baboyi kobongola mitema.
3,9. Yoane afundi ba-Yuda ba lole­ndo mpo bakanisi ’te awa bazali bana ba Abarama, ata basali mabe, bakobika.
3,11. Yoane Mobatisi azalaki kobatisa bato bobele na mai ; kasi ayebisi ’te Yezu ‘akobatisa na Elimo Santu mpe na móto’ ; lokola ’te : na yango milimo mya bango mikopetwama na masumu, mpe bakokoma bana ba Nzambe.
4,1. Zabolo ameki kobebisa mosala mwa Nzambe mpe mwa Mesiya, yango wana asenginyi Yezu ; alakisi Mesiya mambi manso ma lokumu ma mokili, kasi Yezu andimi mango te, mpo alingi kolanda makanisi ma bato te ; ayei kobikisa bato se na botosi Tata.
4,23. Sinagoga : o ndako eye ba-Yuda bakosanganaka mpo ya kosambela o mikolo mya sabato.
4,24. Bokono bwa bonsinga : malali ma ndeke.
5,1. Se lokola Moze abutaki ngomba mpo ya koyamba mibeko mya Nzambe (Bob 24,13), Yezu abuti mwa ngomba mpo ya kosakola mobeko mwa sika.
5,3. Bato ba motema bobola : bato ba bosawa, baye bamiyebi o maboko ma Nzambe.
5,4. ‘Mokili’ balakelaka Israel : ele­mbo ya libiki Mesiya akoyela bango.
5,8. Motema peto : baye bazali na mayele mabe te.
5,17. Mobeko to maloba ma baprofeta : Na mambi manso ma Bondeko bwa Kala Nzambe alengeli Bondeko bwa Sika.
5,18. Mobeko mwa kala motondaki na bitinda ndenge na ndenge. Yezu akokisi mwango, kasi atii mobeko mwa bolingi liboso lya bitinda binso.
5,22. Ligboma : moto aboyi Nzambe.
5,32. Moto moko te akoki kolongola mwasi wa ye soko abalaki engebene na Mobeko mwa Moze (tala mpe Mt 19,9). Kasi moto afandi libala lipekisami na mobeko asengeli kolongola mwasi wa ye (Lv 18,1-20 ; 20,8-21).
5,34.	–Tala Iz 66,1.
–Bolaya soki te : Yezu apekisi kolaya mbala na mbala ; bakoki kolaya ndai soko ntina enene ezali (Tala Mt 23,16-22 ; 26,63).
5,39. Yezu apekisi te ’te moto apalela na bosembo moninga oyo asali mabe (Tala Yo 18,22).
5,43. Koyina monguna etindamaki na mobeko moko te. Kasi o ntango ya Yezu mangomba masusu mazalaki kotinda bato ba bango bakinola basumuki banso, mpe basala bango malamu te.
5,44. Bolinga banguna : lokola ’te « Bosala bango malamu mpe botombela bango bolamu », zambi tokoki kolinga monguna na motema moko te, lokola tokolingaka baninga.
5,46. Ba-Yuda bazalaki kokinola bafutisi mpako, mpo bazalaki kofutisa bato na bosembo te, mpe bazalaki koyokana na ba-Roma.
6,11. Bilei : Manso tosengeli na mango.
6,12. Tosengeli koloba « tokolimbisa », lokola ’te « tolingi kolimbisa ». Totika koloba « tokolimbisaka ». Tondima na bosembo ’te kolimbisa ezali momeseno ata ya moto moko te.
6,13. Na yango Kristu alakisi bayekoli basenge Nzambe ’te alendisa mpe abatela bango.
6,23. Soko moto akomi na lolanda o motema, ezali mpo aboyi koyamba mwinda mwa Nzambe. Akomi bongo na lolanda loleki lolanda la miso na bobe.
6,24. Moto akolukaka mosolo na lokoso mpe na lisumu, akomi moombo wa solo. Mokristu, mwana wa Nzambe, akoki kozala moombo wa mosolo te.
6,25. Yezu apekisi te ’te topangana na mambi masengeli na bomoi bwa nzoto, kasi akebisi biso ’te bokanisi mambi maye bobebisa to boboma bomoi bwa molimo te.
7,1. –Yezu apekisi kokitisa moni­nga, mpo tokoki koyeba bobele misala mya ye, toyebi motema mwa ye te.
–Basambisa bino te : Nzambe akitisa bino te.
7,6. Biloko bisantu biye bizali mateya ma Nsango Elamu. Liboso lya koteya mango, esengeli ’te balengele motema mwa moto ; soki te, akoki koseke to kotuka mango.
7,15. Bateyi ba lokuta : baye bakokosaka bato, mpo bazalaki koluka se bolamu bwa bango moko.
7,22. Mokolo Nzambe akokata makambo ma bato banso.
7,29. Bazalaki koteya polele te, awa bazalaki komibomba nsima ya makambo bankoko batikela bango.
8,3. O miso ma ba-Yuda bato ba maba bazali na mbindo. Yezu akoki kobikisa bato o bokono bwa nzoto, kasi na makamwisi asali alakisi ’te akoki mpe kobikisa milimo mya bato.
8,4. Na makamwisi ma ye Yezu alakisi ’te alingi bato mpe akoyokelaka bango ngolu ; azali mpe komiyebisa lokola Mesiya mpe Mwana wa Nzambe ; kasi alingi te ’te nsango ya makamwisi ma ye epalangana, mbele bato baike bakomono ye lokola mokonzi wa mokili moye ; nzokande ayei lokola Mosaleli wa Yawe asengeli koyoka malozi mpe kokufa.
–Bato ba maba bakokaki kofanda esika yoko na bato basusu te, mpo ’te bango mpe bakoma na maba te. Soko moto wa maba abiki, engebene na mobeko, asengeli komilakisa na nganga Nzambe, oyo akopesa ye monkanda mokolingisa ye kosala misala minso lo­kola bato basusu. Atindamaki mpe kobonza libonza lya botondi o tempelo.
8,5. Komanda oyo wa limpinga lya ba-Roma azalaki na basoda monkama. Akokaki kozala mo-Yuda te.
8,8. Lokola bakotosaka mokonzi wa basoda, ata alobi se liloba lyoko.
8,10. Bipai binso Yezu azali kosenge boyambi : Na bosawa bwa solo, bomipesa o maboko ma Nzambe.
8,12. Bana ba Bokonzi, ezali nde ba-Yuda, baye Nzambe alengeleki mpo ’te bayamba mateya ma Yezu mpe bayingela o Bokonzi boye Yezu asangeli. Bapagano (baye bazali ba-Yuda te) bakoyamba bakoyingela o Bokonzi o esika ya ba-Yuda baye baboyi koyamba.
8,20. ‘Mwana wa Moto’ : Yezu azalaki komipesa nkombo eye mpo ya kolakisa ’te azali moto solo, lokola bato banso, tika se lisumu. Na yango azalaki mpe kokundola maloba ma profeta Daniel (7,13-14), esika ekomami ’te Nzambe akopesa Mwana wa Moto bokonzi.
8,22 Yezu atangi awa « bawei » baye baboyi kobongola mitema, bongo bakozwa bomoi bwa sika o Bokonzi bwa Nzambe te (tala mpe 10,39 ; 16,25-26).
8,29. O mokili moye bazabolo bakoki kokotela bato mpe koyokisa bango mpasi. Ata bokonzi bwa bango bokosuka seko o nsuka ya molongo, Yezu alingi kolonga bango na bobe­ngani bango mpe na bobikisi bato ; apesi mpe bayekoli ba ye likoki lya kosala yango.
9,1. –Moto wa mbengo : moto akufa makolo.
–Mboka ya ye : Petro azalaki na ndako o Kafarnaum.
9,2. Yezu aluki kobikisa molimo mwa moto liboso lya nzoto ya ye. Na maloba ma ye alakeli moto ’te akobika.
O miso ma ba-Yuda lisumu lizalaki ntina ya bokono bwa bato.
9,9. O eleko ya Yezu ba-Roma baponoko bato mpo ya kofutisa mpako (batakoli). Kasi ba-Yuda bazalaki koyina ba-Roma na bafutisi mpako ; bazalaki kotala bango lokola basumuki mpe bato babe.
9,15. O Bondeko bwa Kala balakisi eleko ya Mesiya lokola limpati lya libala.
–Yezu azali lokola mobali oyo Nzambe akobalisa ekolo ya ye. Mbele Yezu alingaki koyebisa na bobombami ’te liwa lya ye lizalaki lisusu mosika te.
9,17. Vino ya sika : ‘mayele ma sika’ (ma Kristu), makoki kozwama epai ya ba-Farizeo te.
9,18. Nkumu : mokonzi wa sinagoga : mbele ezalaki Yairo (Tala Mk 5,22).
9,27. Mwana wa Davidi : nkombo bakopesaka Mesiya (Mobikisi), ut’o eleko ya baprofeta. Asengelaki kobotama o libota lya Davidi.
9,36. Lokola ’te : Ba-Yuda bazalaki na mokambi moko wa solo o makambo ma Nzambe te.
10,2. Apostolo : lokola ’te : moto ati­ndami.
10,5. Sikawa bapostolo basengeli kosakola boyei bwa Bokonzi bwa Nzambe epai ya bato ba Israel, ekolo eye eponomi na Nzambe.
10,12. Na bopesi moto losako, ba-Yuda bazalaki kotombela ye boboto.
10,14. O ekolo ya ba-Yuda, o esika moto wa mobembo ayambamaki malamu te azalaki kolongola mputulu ya makolo mpo ya kolakisa ’te azalaki lisusu na likambo lyoko te na bato ba ndako ena.
10,23. O bisika mingi ‘Mokolo Mwana wa Moto akoya’, elakisi ‘o nsuka ya molóngó’, kasi awa elakisi ’te Mesiya akomiyebisa na nguya ya ye o mokolo akosekwa.
10,25. Belzebul : nzambe wa lokuta wa ba-Kanana, bapagano bafandaka o mokili ntango Nzambe akotisi ba-­Yuda kuna. Ba-Yuda bakotangaka mpe zabolo na nkombo ena.
10,28. Bato bakoki kobotolo bomoi bwa nzoto, kasi bomoi bwa molimo te. Likambo tosengeli kobanga : kotikala na lisumu, yango nde lokola ’te : kondima ’te tozanga bomoi bwa molimo.
10,32. O nsuka ya molóngó, mokolo Mwana akokamba baponomi banso epai ya Tata.
10,34. Yezu alingi boboto mpe boyokani, kasi boswani mpe bokabwani bokozala o ntei ya bato, zambi basusu bakondima ye, basusu bakoboya ye.
10,39. Bomoi : na bomoi bwa mokili moye ná mpe bomoi bwa seko.
10,42. Na maloba maye Yezu alakisi bapostolo. Tala mpe Mt 18,1-6 ; 10,14 ; Mk 9,41.
11,1. Bingumba bya ba-Yuda.
11,3. Oyo asengeli koya : lokola ’te Mesiya. Yoane atii ntembe te, kasi alingi ’te Yezu amiyebisa lokola Mesiya o miso ma bayekoli ba ye. Mbele akanisaki ’te mosala mwa liboso lya Mesiya mozalaki kotumbola basumuki, nzokande Yezu azalaki koyokela bango ngolu.
11,5. Yezu ayanoli Yoane ’te na manso azalaki kosala azalaki kokokisa maloba ma baprofeta. Atangeli ye Iz 26,19 ; 29,18 ; 35,5-6 ; 61,1.
11,6. Lokola ’te : akoboya koyamba ngai te.
11,11. Bondeko bwa Sika boleki Bondeko bwa Kala. Yoane azali profeta wa nsuka wa Bondeko bwa Kala.
11,12. Na makasi : bato ba mpila bakosalaka manso mpo ya koyingela o Bokonzi bwa likolo, kanisa bafutisi mpako na basumuki (tala mpe Lk 7,29 ; 16,16).
11,14. Profeta Malakia (3,23) asakolaki ’te Elia asengelaki kozonga mpo ya kolengele boyei bwa Mobikisi. Yezu alimboli yango boye : Yoane asali yango o esika ya Elia.
11,16. Ba-Yuda baulani na bana bazali mbala mingi na bilulela ; bandimi Yoane te, Yezu mpe te, ata banso babale bazali batindami ba Nzambe.
11,19. Yezu apaleli ba-Yuda, mpo bazalaki kosala lokola bana : ntango bazali kosakana, mosika te bakobanda koswana. Se bongo ba-Yuda baboyi ntoma moko moko oyo Nzambe atindeli bango, ezala Yoane oyo apaleli bango na miso makasi, ezala Yezu oyo alakisi bango motema ngolu.
11,25. Bato bake : bato ba bosawa.
Bato ba bwanya : ba-Farizeo na bateyi ba Mobeko bazali komitia likolo lya baninga. Yango wana bakoki koyoka ntina ya mateya ma Yezu te.
11,30. Ekangiseli : ba-Yuda bazalaki komeme mokumba o motó te, kasi na ekangiseli o mapeke. Tala Yer 6,16 ; Mos 3,17 ; Nz 34,19.
Mokumba mwa Yezu : mitindo mya ye. 
12,2. Kobuka mbuma lokola bayekoli basalaki emononi o miso ma ba-Farizeo lokola mosala monene mwa bobuki mbuma inso ya bilanga : ata mbuma boni, mosala moye mopekisami o mokolo mwa sabato.
12,5. Banganga Nzambe bakosalaka mosala o Tempelo mokolo mwa sabato, bakosala lisumu te. Mobeko motindi bato banso batika mosala o mokolo mwa sabato, kasi banganga Nzambe basengeli kobonza mabonza.
12,8. Yezu ayebisi polele ’te azali mpe mokonzi wa mibeko miye Nzambe apesi ba-Yuda.
12,10. Anyela loboko : loboko lokauka.
12,24. Belzebul ezali Nkumu Baal, moko wa banzambe ba ba-Kanana. Kasi o ntango ya Yezu, ba-Yuda bakomi kotanga Satana na nkombo eye.
12,32. Bakolimbisa ye te, zambi moto aboyi koyamba libiki, amipimi bolimbisi.
12,39. Yezu alobi ‘bato ba bokali’ lokola tokoki kotanga o Oz 1,2.
12,46. Bandeko : Ndeko wa Yezu Yakobo azali mwana wa Alfeo (tala 10,3) mpe wa Maria Kleofas (tala 27,56). Ba-Yuda bakobengaka mpe ‘bandeko’ bana babotami na bandeko ba baboti (Lib 13,8 ; 14,16 ; 29,15 ; Lv 10,4 ; 1 Mkl 23,22 ; Mt 13,55 ; Yo 7,3 ; Bik 1,14 ; 1 Kor 9,5 ; Gal 1,19), lokola biso mpe tokosalaka awa o Afrika. Tala mpe Yo 19,26-27.
12,50. Yezu akinoli bondeko bwa makila te, kasi apusi kokumisa bondeko bwa bana ba Nzambe (tala mpe 10,37).
13,3. Mateo asangisi awa miulani 
nsambo mikolakisa mobombamo mwa Bokonzi bwa Nzambe : ndenge Bokonzi bona bozali, boniboni bokobima o mokili mpe bokokola.
13,11. Mateya na miulani mazali kolakisa Bokonzi bwa Nzambe polele mpenza te. Bayoki baike bakoki koyeba ntina mpenza te, mpo bayebi te boniboni Mesiya asengeli koyoka mpasi mpe kokufa.
13,25. Mabe na mokili mauti epai ya Nzambe te.
13,33. Solo, lokola mboto eke mpe nkisi ya mampa, Bokonzi bwa Nzambe bokokolaka se bokoli, kasi se malembe.
13,52. Moteyi wa Mobeko oyo akomi moyekoli wa Yezu, azwi mateya o Buku ya Mobeko mpe lisusu o maloba ma Yezu.
14,19. Mateo mpe, lokola Luka na Yoane, atali likambo liye lya boleisi bato ebele lokola elembo ya Ukaristya. Yango wana akomi : akamati mampa, abenisi, abuki, apesi.
15,2. Mikomami o Buku ya Mobeko te.
15,3. Na Mibeko mya Nzambe ba-Yuda babakisi mitindo ebele (bikila 365 na mitindo 248) mpo ya kosalisa bato o botosi Mibeko. Kasi na ebele ya mitindo miye ba-Yuda mingi bakomaki kobunga ntina enene ya Mibeko zomi mya Nzambe.
15,5. Biloko bibonzami na Nzambe bikoki kosalisa bato lisusu te. Bana babe batikali bongo na biloko bya bango mpe baboyi kosalisa baboti.
15,24. Ata Yezu atindami liboso kobikisa ba-Yuda, aboyi kosalisa bapagano te. Abikisi mwana wa mompaya oyo, mpo amoni ’te azalaki na boyambi.
15,26. Ba-Yuda bazalaki komitanga bana ba Nzambe ; o miso ma bango bapagano bazalaki ‘mbwa’. Yezu alobi awa lokola ba-Yuda bameseni, kasi mpo ya kolakisa ’te aboyi bango te alobi : ‘bana ba mbwa’.
16,3. Bilembo : baboyaki kondima makamwisi ma Yezu, maye malakisi boyei bwa Mesiya mpe bwa Bokonzi bwa Nzambe (tala mpe 11,5 ; 12,28).
16,4.	–Bato bakotosaka Nzambe te : Yezu alobi : « Bato ba bokali ». Tala Mt 12,39 +.
–Elembo ya Yona : nsekwa.
16,6. Nkisi ya mampa, soko ezali malamu ekovimbisa mampa, kasi soko ezali mabe ekokomisa mampa mobimba bololo.
16,14. Ba-Yuda baike bakanisaka ’te Elia to profeta mosusu akozonga o eleko ya Mesiya.
16,16. Petro aleki basusu na boyambi : ayebi ’te Yezu azali lisanga na Nzambe na ndenge ya solo, mpo azali solo Mwana wa ye.
16,17. Elakisi moto na botau bwa ye bonso.
16,18.	–Petro : ekomami awa o lokota la ba-Aramea, Kefas.
–Bobele na nkombo Eklezya ba-­Yuda bazalaka kotanga libota lya bana ba Nzambe o Bondeko bwa Kala.
–Ata liwa : Mateo akomi : ‘Bizibeli bya limfelo’ : Bizibeli bya mboka bilakisi bokasi bwa yango ; ‘bizibeli bya limfelo’ bilakisi bokasi bwa lisumu mpe bwa liwa.
16,20. Yezu alingi te ’te bato basusu bayeba ’te azali Mesiya, mbele bakopono ye bo mokambi wa batomboki baye balingi kobengana ba-Roma.
16,23. Ata boni, Petro abombi elikya ’te Yezu akoyambama bo mokonzi o Yeruzalem. Se lokola Satana o eliki, ntango alakelaki Yezu nkita mpe lokumu, Petro azalaki kosenginya Yezu. Yango wana Yezu atangi ye Satana.
16,28 : Bakomono nsuka ya Yeruzalem (tala 24,2).
17,1. Ngomba Tabor.
17,3. Moze na Elia : Bantoma ba Mobeko mpe baprofeta bakumisaka mobandi wa Bondeko wa Sika.
17,21. (Kasi bakoki kobengana zabolo ya ndenge ena bobele na losambo mpe na bolali nzala. Tala Mk 9,29.)
17,24. Ba-Yuda bakofutaka ba-­Roma mpako ; bakofutaka mpe ntako ya Tempelo ekokani na lifuta lya mokolo moko.
18,11. Mbele mbakisa eye euti epai ya Luka (19,10).
18,17. Soko ndeko moko aboyi koyoka baye bapaleli ye mpo ya likambo linene asali, bakristu bakima ye, se lokola ba-Yuda bazalaki kosala na bapagano mpe na bafutisi mpako.
18,18. Yezu apesaki Petro likoki lyango (16,18) ; sikawa akopesa lyango na bapostolo ba ye zomi na babale.
18,21. Mbala nsambo : mbala ebele.
18,22. Mbala ntuku nsambo mbala nsambo : mbala na mbala lisusu !
18,24. Nyongo enene mpenza ! Biso mpe tozali na nyongo enene boye mpo ya masumu ma biso !
18,28. Nyongo enene mpenza te. Denario yoko ezali lokola lifuta lya mokolo moko.
19,3. O ntango ya Yezu bateyi ba Mobeko basusu bazalaki kondima ’te bakoki koboya mwasi bobele mpo ya ntina enene, basusu bazalaki kondima yango ata mpo ya ntina ya mpamba. Yezu asukisi ntembe nyonso nye : ata na ntina nini bakoki koboya mwasi !
19,9. Tala mpe 5,32 : ezali libala mpenza te.
19,12. Bato basusu bakobala te, mpo balingi kosalela Bokonzi bwa Nzambe. Nzambe moto akabeli bango likabo liye lokola ngrasya.
19,17. ‘Se Nzambe’. Mateo akomi : ‘Se moto moko’ kasi tokomi ‘Nzambe’ mpo ya maye totangi o Mk 10,18 mpe o Lk 18,19.
19,24. 	–Liboso bazalaki kobongola molongo moye boye : ‘Kamela ekoleka o lilusu’ nzokande o greke ekomami ‘kamilos’ (nsuki ya kamela, to mokulu mosalemi na nsuki ya kamela), kasi ‘kamèlos’ (kamela) te.
–Moto akangemi na nkita wa ye akoyingela o Bokonzi bwa Likolo te.
20,1. Nzambe azali nta bolamu, akokabelaka makabo ma ye, lokola ye moko alingi. Baye babyangemi liboso bayokela baye babyangemi nsima zuwa te (tala mpe Rom 9,14).
20,2. Denario : lifuta lya mokolo moko.
20,22. O Biblia bakokomaka bongo soko moto asengeli koyoka mpasi mingi, lokola ’te Nzambe akomelisa ye nkeni etondi na limeli bololo.
20,28. Ebele : bato mingi bakobika, nzokande mobikisi azali se moko (tala Iz 53,11).
21,5. Tala Iz 62,11 mpe Zak 9,9. O Palestina babola bazalaki kobutela mpunda, bato ba nkita bazalaki kotambwisa farasa.
21,9. –« Ozana » : Kala ezalaka « Bikisa ko ! » Nsima ekomi liloba lya bokumisi to lya bosengi bopamboli.
–Tala Nz 118,25-26.
21,17. Betania : mboka na km 3 epai ya Esti ya Yeruzalem.
21,19. Nzete ya figi : Yezu alakisi Israel awa (tala Yer 18,1).
21,23 : Likoki lya kobengana bato ba mombongo o Tempelo (21,12-17).
21,28. Bana babale : Ba-Yuda baike epai, mpe bafutisi mpako na basumuki epai.
21,33. Basali ba elanga : basali lokola ba-Israel baike bazalaki kosala na baprofeta mpe na Kristu yemei. Tala mpe Iz 5,1. O moulani moye nkolo ezali Nzambe, elanga ya vino ezali ekolo ya ba-Yuda, basaleli nde baprofeta, mpe mwana wa nkolo ezali Yezu. Na bobombami Yezu asili ayebisi awa ’te bakoboma ye.
22,1. O moulani moye mokonzi ezali Nzambe, limpati lya libala ezali Bokonzi bwa Nzambe, mwana wa mokonzi ezali Yezu, mpe baye babyangemi o limpati lya libala mpe baboyi koya, ezali bato baboyi koyamba libiki Yezu ayeli bango. Baye bazwami o nzela ezali basumuki na bapagano ; mboka etumbami elakisi nsuka ya Yeruzalem o mobu mwa 70.
22,11. Elamba moto akolata o mokolo mwa nsuka ya molongo : motema mwa ye mosengeli kobongo.
22,16. Bato ba Erode batindami mpe, zambi bazalaki kondima ba-­Roma ; bango bameka kotiela ye motambo, bongo bakoka kofunda Yezu mpe epai ya ba-Roma.
22,20. Mokonzi monene moko moko wa Roma azalaki kobimisa bibende bya mosolo na elongi mpe na nkombo ya ye.
22,45. Mokonzi : Yezu atangi maloba ma nzembo eye mpo ya kolakisa ’te Mesiya azali Mokonzi wa Davidi, awa afandi o loboko la mobali la Nzambe.
23,2. Bafandi o kiti ya Moze : elakisi ’te bazwi likoki lya koteya.
23,5. Ba-Farizeo bazalaki kolata mwa libenga o motó ; o kati ya libenga lyango bazalaki kotia mwa monkanda na maloba ma Bikoma Bisantu, mpo ya kolakisa ’te bakokanisaka maloba ma Nzambe ntango inso.
23,7. ‘Rabi’ lokola ’te Moteyi.
23,9. Yezu aboyi nkombo ya ‘tata’ te, kasi alingi te ’te bato bamikumisa na yango.
23,14. « Mawa na bino, bateyi ba Mobeko na ba-Farizeo bakibisi, bokobotoloko biloko bya basi bazenge, mpe bolingi kolakisa bato ’te bokosambelaka mingi. Ya solo, bokozwa etumbu enene mpenza ! » Tala Mk 12, 40).
23,32 : Na bobomi Kristu.
23,33 : Etupa : nyoka eke.
23,35. Zakaria : Tala Mkl 24,20-22.
23,38. Ndako ya yo : Tempelo.
23,39. Mokolo Kristu akozonga na nkembo (Tala Nz 118,26 ; Bik 2,33).
24,2. Ba-Roma bakoboma Yeruzalem mibu 40 na nsima, o mobu mwa 70.
24,3. Bapostolo bakanisi ’te nsuka ya Yeruzalem ekozala mpe nsuka ya molóngó.
24,7. Mokili moningani : Tala Iz 8,21 ; 13,13 ; 19,2 ; Yer 21,9 ; 34,17 ; Ez 5,12 ; Am 4,6-11 ; 8,8 ; 2 Mkl 15,6.
24,8. Na maloba maye baprofeta basili kolakisa eleko ya nsomo ekoya liboso lya Bokonzi bwa Mesiya : Iz 13,8 ; 26,17 ; 66,7 ; Yer 6,24 ; 13,21 ; Oz 13,13 ; Mi 4,9-10.
24,15. Na nkombo eye profeta Daniel atangaka ekeko mokonzi Antioko Epifane atiaki o Tempelo (Dan 9,27 ; 11,31 ; 12,11). Yezu atangi ba-Roma na nkombo eye : o mobu mwa 70 bakoboma Yeruzalem na mpe Tempelo.
24,22. Baponomi : Ekoki kozala baye bakoyingela o likolo, to baye bakobika o mokolo bakoboma Yeruzalem.
24,28. Monguna moko te akokima etumbu.
24,29. Baprofeta basangelaka etumbu ya banguna ba Israel bobele na maloba mana (Am 8,9 ; Iz 13,9-10 ; 34,4 ; Za 12,10-12). O buku ya Boyebisi Mibombamo (1,7) Yoane akomi mpe makambo maye ma nsomo.
24,30. Na maloba maye Daniel (7, 13-14) ayebisi boyei bwa Mwana wa Moto.
24,34. Nsuka ya Yeruzalem, kasi nsuka ya molóngó te !
24,36. Na mayele ma ye ma bomoto Yezu ayebi yango te. Likambo liye litali mpe mosala mwa libiki te. 
24,37. Mateo abakisi maloba maye ma Yezu mpo ’te biso banso tokengele, zambi Yezu akokata likambo lya moto moko moko o ntango ya liwa.
24,42-44. Yezu alobi awa mpo ya mosaleli wa Kristu, akozala na mosala o Eklezya.
25,1. Bilenge basi zomi : bakristu bazali kozila boyei bwa Kristu. Bakeba ’te mwinda (mwa bokengeli) mwa bango mozima te.
25,14. Talenta ezalaki na motuya mosengeli mpo ya kosomba kilo 34 ya wolo ! Mokristu moko moko akose­ngela kolakisa mbuma ya makabo manene Nzambe apesi ye.
25,21. Esengo eye ekozala o Bokonzi bwa Likolo.
25,32. Bato ba bikolo binso mpe ba ntango inso ; bakosangisa bango banso o mokolo mwa nsekwa ya nsuka.
25,35-36. Moto nyonso akomipesaka na bosalisi baninga na misala ndenge na ndenge, akoyambama o mboka Tata. Tosengeli mpe kosakola boyambi bwa biso o miso ma bato.
26,7. 	–Maria, ndeko wa Marta na Lazaro (tala Yo 12,1-3).
–Mbélé : porcelaine.
26,12. Yezu akumisi Maria mingi, zambi o miso ma ba-Yuda bokundi moto bozali mosala mopusi bolamu.
26,15. Tala Za 11,12 : na mitako ntuku isato bazalaki kofuta moombo (Bob 21,32).
26,17. 	–Eyenga ya Mampa mavimbi te ezalaki koumela poso mobimba ; ebandaki o mokolo mwa Pasika.
–Eyenga ya Pasika : tala Bob 21,1-14 ; 12,21-28 ; 12,43-50.
26,23. Elakisi : moko wa bapostolo bazali kolia na ye.
26,28. Makila ma bondeko : Lokola o ngomba Sinai makila masopani mpo ya bokati Bondeko bwa Kala (Bob 24,4-8) ; makila ma Yezu makosopama mpo ya bokati Bondeko bwa Sika (Yer 31, 31).
26,30. Nsima ya limpati lya Pasika ba-Yuda bazalaki kotanga Nzembo ya botondi (Nz 113 tee 118) mpo ya kokundola ndenge Nzambe abikisi bango o boombo bwa Ezipeti.
26,31. O miso ma bapostolo, Mesiya akoki kondima te ’te baboma ye.
26,39. Liwa lizalaki kobangisa Yezu mpe.
26,50. Oyei awa mpo ya kokaba ngai.
26,54. Baprofeta ba solo bakonyo­kwamaka mingi, basusu bakoboma­ma­ka. O Iz 53,2-12 tokoki kotanga nde­nge ‘mosaleli wa Yawe akoyoka mpasi’.
26,61. Yezu alobaki bongo te. Ayebisaki ’te bakoboma Tempelo (Mt 24,2) ya kala. Na nsekwa ya ye akomonono lokola Tempelo ya sika (Yo 2,19-22).
26,64. 	–Yezu asakoli polele ’te azali Mesiya, oyo Daniel (7,13) mpe mokomi wa Nzembo (110,1) bayebisaka.
–Oyo aleki nguya : Nzambe.
26,65. Atuki Nzambe : zambi amipesi lokumu la Nzambe.
26,73. Petro azalaki mo-Galilea. Elobeli ya ye ekeseneki na eye ya bato ba Yudea.
27,2. Pilato azalaki moyangeli mo-Roma wa Yudea banda 26 kin’o 36. Bobele ye akoki kopesa ndingisa ya kopesa moto etumbu ya liwa.
27,9-10. Maloba maye mazali maloba ma profeta Yeremia mpenza te, mazali ma Zakaria (11,12-13) ; kasi lisolo totangi o Yer 32,6-10 liulani na maloba maye.
27,24. Na bosukoli maboko Pilato alakisi ’te alingi kosopa makila ma moto te, nzokande ye moto atindi ’te baboma Yezu.
27,25. Tondimi ’te biso moko tolingi ’te akufa !
27,26. Liboso lya koboma moto o kuruse, ba-Roma bameseneki kobetisa ye fimbo.
27,33 : Golgota o aram, Kalvario o latini, lokola ’te ‘esika ya ebolo’ (crâne). Mwa ngomba eye ya mabanga pene na lopango la Yeruzalem ezalaki na ndenge ya motó.
27,34. Na bomelisi ye vino ngai eye, balingaki kosalisa Yezu ’te ayoka mpasi makasi te, kasi Yezu aboyi.
27,46. Na maloba ma ebandela ya Nzembo ya Davidi (22,2), Yezu atiaki motema mobimba na Tata wa ye.
27,51. Profeta Amos (8,9) ayebisaka ’te makambo maye makosalema o mokolo mwa Yawe.
27,61. Maria mosusu : mwasi wa Kleo­fas mpe mama wa Yakobo na Yozefu ; azalaki ndeko wa mama wa Yezu.
27,62. Bolengeli : Mokolo liboso lya sabato. Na ngonga ya 6 ya mpokwa mokolo mosili. Ba-Yuda bakei epai ya Pilato o mokolo mwa sabato na mpokwa.
28,6. Yezu asekwi te mpo abanda lisusu bomoi bwa nse, kasi mpo ya kozwa nkembo o mboka Tata.
28,16. Mateo ayebisi ndenge Yezu abimelaki bato ba ye o Galilea ; Luka ayebisi ndenge abimelaki bango o Yeruzalem